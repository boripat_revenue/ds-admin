object dm: Tdm
  OldCreateOrder = False
  Left = 363
  Height = 570
  Width = 531
  object db: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=.\db.' +
      'mdb;Mode=Share Deny None;Extended Properties="";Jet OLEDB:System' +
      ' database="";Jet OLEDB:Registry Path="";Jet OLEDB:Database Passw' +
      'ord=9999;Jet OLEDB:Engine Type=5;Jet OLEDB:Database Locking Mode' +
      '=1;Jet OLEDB:Global Partial Bulk Ops=2;Jet OLEDB:Global Bulk Tra' +
      'nsactions=1;Jet OLEDB:New Database Password="";Jet OLEDB:Create ' +
      'System Database=False;Jet OLEDB:Encrypt Database=False;Jet OLEDB' +
      ':Don'#39't Copy Locale on Compact=False;Jet OLEDB:Compact Without Re' +
      'plica Repair=False;Jet OLEDB:SFP=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 40
    Top = 24
  end
  object dscConfig: TDataSource
    DataSet = tblConfig
    Left = 120
    Top = 80
  end
  object tblConfig: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'configs'
    Left = 40
    Top = 80
    object tblConfigID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object tblConfigClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblConfigTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblConfigshow_news: TBooleanField
      FieldName = 'show_news'
    end
    object tblConfigshow_ads: TBooleanField
      FieldName = 'show_ads'
    end
    object tblConfigVideo_source: TBooleanField
      FieldName = 'video_source'
    end
    object tblConfigDefault_video: TWideStringField
      FieldName = 'Default_video'
      Size = 100
    end
    object tblConfigvideo_directory: TWideStringField
      FieldName = 'video_directory'
      Size = 255
    end
    object tblConfighttp_server: TWideStringField
      FieldName = 'http_server'
      Size = 100
    end
    object tblConfigvideo_program: TWideStringField
      FieldName = 'video_program'
      Size = 255
    end
    object tblConfignews_program: TWideStringField
      FieldName = 'news_program'
      Size = 255
    end
    object tblConfigrefresh_time: TIntegerField
      FieldName = 'refresh_time'
    end
    object tblConfigftp_server: TWideStringField
      FieldName = 'ftp_server'
      Size = 200
    end
    object tblConfigftp_port: TIntegerField
      FieldName = 'ftp_port'
    end
    object tblConfigftp_user: TWideStringField
      FieldName = 'ftp_user'
      Size = 50
    end
    object tblConfigftp_pass: TWideStringField
      FieldName = 'ftp_pass'
      Size = 50
    end
    object tblConfigftp_path: TWideStringField
      FieldName = 'ftp_path'
      Size = 100
    end
    object tblConfigftp_passive: TBooleanField
      FieldName = 'ftp_passive'
    end
    object tblConfigdebug_mode: TBooleanField
      FieldName = 'debug_mode'
    end
    object tblConfigsys_pwd: TWideStringField
      FieldName = 'sys_pwd'
      Size = 255
    end
    object tblConfignews_font: TWideStringField
      FieldName = 'news_font'
      Size = 255
    end
    object tblConfignews_size: TIntegerField
      FieldName = 'news_size'
    end
    object tblConfignews_color: TIntegerField
      FieldName = 'news_color'
    end
    object tblConfignews_isbold: TIntegerField
      FieldName = 'news_isbold'
    end
    object tblConfignews_isitalic: TIntegerField
      FieldName = 'news_isitalic'
    end
    object tblConfignews_isunderline: TIntegerField
      FieldName = 'news_isunderline'
    end
    object tblConfigbanner_mode: TIntegerField
      FieldName = 'banner_mode'
    end
    object tblConfigbanner_show_interval: TIntegerField
      FieldName = 'banner_show_interval'
    end
    object tblConfignews_delay: TIntegerField
      FieldName = 'news_delay'
    end
    object tblConfignews_height: TIntegerField
      FieldName = 'news_height'
    end
    object tblConfigbanner_width: TIntegerField
      FieldName = 'banner_width'
    end
    object tblConfignews_back_color: TIntegerField
      FieldName = 'news_back_color'
    end
    object tblConfignews_step: TIntegerField
      FieldName = 'news_step'
    end
    object tblConfigbanner_prop: TBooleanField
      FieldName = 'banner_prop'
    end
    object tblConfigshow_clock: TBooleanField
      FieldName = 'show_clock'
    end
    object tblConfigrss_active: TBooleanField
      FieldName = 'rss_active'
    end
    object tblConfigrss_url: TWideStringField
      FieldName = 'rss_url'
      Size = 255
    end
    object tblConfiglogo_width: TIntegerField
      FieldName = 'logo_width'
    end
    object tblConfiglogo_stretch: TBooleanField
      FieldName = 'logo_stretch'
    end
    object tblConfiglogo_ratio: TBooleanField
      FieldName = 'logo_ratio'
    end
    object tblConfigdef_type: TIntegerField
      FieldName = 'def_type'
    end
    object tblConfigdef_file: TWideStringField
      FieldName = 'def_file'
      Size = 128
    end
  end
  object qry: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    Left = 360
    Top = 24
  end
  object dsc: TDataSource
    AutoEdit = False
    DataSet = qry
    Left = 408
    Top = 24
  end
  object dscNews: TDataSource
    DataSet = tblNews
    OnStateChange = dscNewsStateChange
    Left = 120
    Top = 152
  end
  object tblNews: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'id'
    TableName = 'news'
    Left = 40
    Top = 152
    object tblNewsID: TIntegerField
      FieldName = 'ID'
    end
    object tblNewsClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblNewsTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblNewsHeadLine: TWideStringField
      FieldName = 'HeadLine'
      Size = 255
    end
    object tblNewsDesc: TWideStringField
      FieldName = 'Desc'
      Size = 255
    end
    object tblNewsStartDate: TDateTimeField
      FieldName = 'StartDate'
    end
    object tblNewsStopDate: TDateTimeField
      FieldName = 'StopDate'
    end
    object tblNewsActive: TBooleanField
      FieldName = 'Active'
    end
    object tblNewsCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblNewsCDate: TDateTimeField
      FieldName = 'CDate'
    end
  end
  object dscQ: TDataSource
    DataSet = tblQ
    OnStateChange = dscQStateChange
    Left = 120
    Top = 216
  end
  object tblQ: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'id'
    TableName = 'queue'
    Left = 40
    Top = 216
    object tblQID: TIntegerField
      FieldName = 'ID'
    end
    object tblQQueue_no: TWideStringField
      FieldName = 'Queue_no'
      Size = 255
    end
    object tblQClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblQTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblQQName: TWideStringField
      FieldName = 'QName'
      Size = 255
    end
    object tblQDesc: TWideStringField
      FieldName = 'Desc'
      Size = 255
    end
    object tblQStartDate: TDateTimeField
      FieldName = 'StartDate'
    end
    object tblQStopDate: TDateTimeField
      FieldName = 'StopDate'
    end
    object tblQActive: TBooleanField
      FieldName = 'Active'
    end
    object tblQCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblQCDate: TDateTimeField
      FieldName = 'CDate'
    end
    object tblQRunning: TBooleanField
      FieldName = 'Running'
    end
    object tblQIsMonday: TBooleanField
      FieldName = 'IsMonday'
    end
    object tblQIsTuesday: TBooleanField
      FieldName = 'IsTuesday'
    end
    object tblQIsWednesday: TBooleanField
      FieldName = 'IsWednesday'
    end
    object tblQIsThursday: TBooleanField
      FieldName = 'IsThursday'
    end
    object tblQIsFriday: TBooleanField
      FieldName = 'IsFriday'
    end
    object tblQIsSaturday: TBooleanField
      FieldName = 'IsSaturday'
    end
    object tblQIsSunday: TBooleanField
      FieldName = 'IsSunday'
    end
    object tblQStartInterval: TDateTimeField
      FieldName = 'StartInterval'
    end
    object tblQEndInterval: TDateTimeField
      FieldName = 'EndInterval'
    end
  end
  object dscQF: TDataSource
    DataSet = tblQF
    OnStateChange = dscQFStateChange
    Left = 120
    Top = 280
  end
  object tblQF: TADOTable
    Connection = db
    CursorType = ctStatic
    OnCalcFields = tblQFCalcFields
    IndexFieldNames = 'QID'
    MasterFields = 'ID'
    MasterSource = dscQ
    TableName = 'queue_files'
    Left = 40
    Top = 280
    object tblQFID: TIntegerField
      FieldName = 'ID'
    end
    object tblQFQID: TIntegerField
      FieldName = 'QID'
    end
    object tblQFClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblQFTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblQFFileName: TWideStringField
      FieldName = 'FileName'
      Size = 255
    end
    object tblQFNewFileName: TWideStringField
      FieldName = 'NewFileName'
      Size = 255
    end
    object tblQFFileType: TWideStringField
      FieldName = 'FileType'
      Size = 255
    end
    object tblQFFileSize: TIntegerField
      FieldName = 'FileSize'
    end
    object tblQFFileLocation: TWideStringField
      FieldName = 'FileLocation'
      Size = 255
    end
    object tblQFActive: TBooleanField
      FieldName = 'Active'
    end
    object tblQFAvail: TBooleanField
      FieldName = 'Avail'
    end
    object tblQFOrdering: TIntegerField
      FieldName = 'Ordering'
    end
    object tblQFCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblQFCDate: TDateTimeField
      FieldName = 'CDate'
    end
    object tblQFFSizeMB: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FSizeMB'
      DisplayFormat = '0.00'
      Calculated = True
    end
  end
  object qryClearQ: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from queue;'
      'delete from old_queue;')
    Left = 208
    Top = 216
  end
  object qryGetActiveQ: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select id, queue_no'
      'from queue'
      'where running')
    Left = 280
    Top = 216
  end
  object qryClearQF: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from queue_files')
    Left = 208
    Top = 280
  end
  object qryGetActiveFile: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select qf.id'
      'from queue q, queue_files qf'
      'where q.id = qf.qid'
      'and q.running'
      'and qf.active'
      'order by ordering')
    Left = 280
    Top = 280
  end
  object qryClearNews: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from news')
    Left = 208
    Top = 152
  end
  object dscPlayQ: TDataSource
    DataSet = tblPlayQ
    Left = 120
    Top = 352
  end
  object tblPlayQ: TADOTable
    Connection = db
    CursorType = ctStatic
    TableName = 'playq'
    Left = 40
    Top = 352
    object tblPlayQID: TIntegerField
      FieldName = 'ID'
    end
  end
  object qryClearPlay: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from playq')
    Left = 208
    Top = 352
  end
  object qryAnotherActiveQ: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'qid'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'select id'
      'from queue'
      'where active'
      'and id <> :qid')
    Left = 376
    Top = 216
  end
  object dscBanner: TDataSource
    DataSet = tblBanner
    OnStateChange = dscNewsStateChange
    Left = 120
    Top = 432
  end
  object tblBanner: TADOTable
    Connection = db
    CursorType = ctStatic
    IndexFieldNames = 'id'
    TableName = 'banner'
    Left = 40
    Top = 432
    object tblBannerID: TIntegerField
      FieldName = 'ID'
    end
    object tblBannerClientID: TIntegerField
      FieldName = 'ClientID'
    end
    object tblBannerTerminalID: TIntegerField
      FieldName = 'TerminalID'
    end
    object tblBannerFileName: TWideStringField
      FieldName = 'FileName'
      Size = 255
    end
    object tblBannerNewFileName: TWideStringField
      FieldName = 'NewFileName'
      Size = 255
    end
    object tblBannerShowType: TIntegerField
      FieldName = 'ShowType'
    end
    object tblBannerShowPosition: TIntegerField
      FieldName = 'ShowPosition'
    end
    object tblBannerFileSize: TIntegerField
      FieldName = 'FileSize'
    end
    object tblBannerFileLocation: TWideStringField
      FieldName = 'FileLocation'
      Size = 255
    end
    object tblBannerStartDate: TDateTimeField
      FieldName = 'StartDate'
    end
    object tblBannerStopDate: TDateTimeField
      FieldName = 'StopDate'
    end
    object tblBannerActive: TBooleanField
      FieldName = 'Active'
    end
    object tblBannerCUser: TWideStringField
      FieldName = 'CUser'
      Size = 50
    end
    object tblBannerCDate: TDateTimeField
      FieldName = 'CDate'
    end
  end
  object qryClearBanner: TADOQuery
    Connection = db
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'delete from banner')
    Left = 208
    Top = 432
  end
  object qryDupQueue: TADOQuery
    Connection = db
    LockType = ltReadOnly
    Parameters = <
      item
        Name = '[SDATE]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = '[FDATE]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = '[bSun]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = '[bMon]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = '[bTue]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = '[bWed]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = '[bThu]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = '[bFri]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = '[bSat]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = '[STIME]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = '[FTIME]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end
      item
        Name = '[MyID]'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      #13'SELECT * FROM ('#10
      ''
      'SELECT '
      '(((StartDate <=[SDATE] And [SDATE]<StopDate) '
      'Or (StartDate <[FDATE] And [FDATE]<=StopDate)) '
      'Or (([SDATE]<StartDate) And ([FDATE]>StopDate))) AS PD,'
      
        ' ((IsSunday=[bSun]) Or (IsMonday=[bMon]) Or (IsTuesday=[bTue]) O' +
        'r (IsWednesday=[bWed]) Or (IsThursday=[bThu]) Or (IsFriday=[bFri' +
        ']) Or (IsSaturday=[bSat])) AS PW,'
      
        ' ((timevalue(StartInterval) <=timevalue([STIME])  And timevalue(' +
        '[STIME]) <timevalue(EndInterval)) '
      
        'Or (timevalue(StartInterval) <timevalue([FTIME]) And timevalue([' +
        'FTIME])<=timevalue(EndInterval)) '
      
        'Or ((timevalue([STIME])<timevalue(StartInterval)) And (timevalue' +
        '([FTIME])>timevalue(EndInterval)))) AS PT,'
      ' id, StartDate, StopDate, StartInterval, EndInterval'
      'FROM old_queue'
      'WHERE (id<>[MyID]) And (Active=True) )AS T1'
      'WHERE (PD=true) AND (PW=true) AND (PT=true);')
    Left = 216
    Top = 488
  end
end
