unit fmFTP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, FileCtrl, AdvGlassButton, RXCtrls, ExtCtrls, DB,
  AdvOfficePager, DBCtrls, Mask, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdFTP, ComCtrls, IdAntiFreezeBase,
  IdAntiFreeze;

type
  TfrmFTP = class(TForm)
    AdminPage: TAdvOfficePager;
    AdvOfficePage2: TAdvOfficePage;
    Bevel3: TBevel;
    RxLabel12: TRxLabel;
    RxLabel13: TRxLabel;
    lblFileUnit: TRxLabel;
    txtFileName: TEdit;
    txtFileShow: TEdit;
    btnFile: TAdvGlassButton;
    btnUpload: TAdvGlassButton;
    OpenDialog1: TOpenDialog;
    RxLabel1: TRxLabel;
    btnCancel: TAdvGlassButton;
    FTP: TIdFTP;
    txtFileOrder: TEdit;
    ProgressBar1: TProgressBar;
    IdAntiFreeze1: TIdAntiFreeze;
    txtFileSize: TEdit;
    procedure btnFileClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnUploadClick(Sender: TObject);
    procedure FTPWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure FTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure FTPWorkEnd(Sender: TObject; AWorkMode: TWorkMode);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetButton(value: boolean);
  end;

var
  frmFTP: TfrmFTP;

implementation

uses
  fmAdmin, data;
{$R *.dfm}

procedure TfrmFTP.SetButton(value: boolean);
begin
  btnFile.Enabled := Value;
  btnUpload.Enabled := Value;
  btnCancel.Enabled := True;
end;

procedure TfrmFTP.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmFTP.FormShow(Sender: TObject);
begin
  SetButton(True);
  txtFileName.Text := '';
  txtFileShow.Text := '';
  txtFileOrder.Text := '0';
  ProgressBar1.Position := 0;
end;

procedure TfrmFTP.btnFileClick(Sender: TObject);
var
  myFile: file of Byte;
begin
  if OpenDialog1.Execute then
  begin
    AssignFile(myFile, OpenDialog1.FileName);
    Reset(myFile);
    txtFileName.Text := OpenDialog1.FileName;
    txtFileSize.Text := IntToStr(FileSize(myFile));
    txtFileShow.Text := FormatFloat('#,##0.00',FileSize(myFile)/1024/1024);
    ProgressBar1.Max := FileSize(myFile);
    CloseFile(myFile);
    if (strtofloat(txtFileSize.Text) > (1024*1024*100)) then
      showMessage('File is too large');
  end;
end;

procedure TfrmFTP.btnUploadClick(Sender: TObject);
var
  cmdstr: string;
  fname, flocation: string;
  bSuccess: boolean;
begin
  bSuccess:=false;
  if txtFileName.Text = '' then
    begin
    MessageDlg('Error: Please select video file!!!', mtError, [mbOK], 0);
    abort;
    end;
  if not FileExists(txtFileName.Text) then
    begin
    MessageDlg('Error: File is not exist!!!', mtError, [mbOK], 0);
    abort;
    end;

  Selectnext(ActiveControl, true, true);
  SetButton(False);
  //Self.Enabled := False;

  with dm.tblQF do
  begin
    MemoryStream.Clear;

    cmdstr := frmAdmin.GenHTTPRequest(true, 6,
//                    dm.tblQ.fieldbyname('id').AsString
                    sQueueID
                    + '&filename=' + frmAdmin.ReplaceSpace(ExtractFileName(txtFileName.Text))
                    + '&filesize=' + txtFileSize.Text
                    + '&ordering=' + txtFileOrder.Text
                    + '&active=Y');

    try
      frmAdmin.ActionLog('Video Upload [S] =>'+cmdstr);
      frmAdmin.IdHTTP1.get(cmdstr, MemoryStream);
      Application.ProcessMessages;

      MemoryStream.Position := 0;
      frmAdmin.Memo2.Lines.Clear;
      frmAdmin.Memo2.Lines.LoadFromStream(MemoryStream);
      Application.ProcessMessages;

    except
      MessageDlg('Error: Cannot connect to server !!!', mtError, [mbOK], 0);
      frmAdmin.ShowLog('Error: Cannot Add File to Server. ' + ExtractFileName(txtFileName.Text));
      abort;
    end;

    ResponseStr := frmAdmin.ExtractResponse(frmAdmin.Memo2.Lines[0]);

    if strtoint(ResponseStr[0]) > 0 then
    begin
      frmAdmin.ActionLog('Video Upload [R] =>'+ResponseStr[0]+'::'+ResponseStr[1]+'::'+ResponseStr[2]);
      flocation := ResponseStr[1];
      fname := ResponseStr[2];

      try
        // ============start FTP part===================
        if FTP.Connected then FTP.Disconnect;
        FTP.Host := ftp_server;
        FTP.Port := ftp_port;
        FTP.Username := ftp_user;
        FTP.Password := ftp_pass;

        FTP.Passive := ftp_passive;
        frmAdmin.ActionLog('Prepare Upload =>');
        FTP.Connect(TRUE, 60);
        Application.ProcessMessages;

        frmAdmin.ActionLog('Upload ['+txtFileName.Text+'] =>'+flocation + '/' + fname);
        FTP.Put(txtFileName.Text, flocation + '/' + fname);       // set file name which get from server
        FTP.Disconnect;

        Application.ProcessMessages;
        MessageDlg('FTP Upload Video finished.', mtInformation, [mbOK], 0);
        frmAdmin.ShowLog('Finish: Upload File to Server. ' + flocation
          + '/' + ExtractFileName(txtFileName.Text) + ' (' + fname + ')');
        frmAdmin.ActionLog('Upload ['+txtFileName.Text+'] => finished');

        Application.ProcessMessages;

        // sending ftp confirmation message to server
        MemoryStream.Clear;
        cmdstr := frmAdmin.GenHTTPRequest(true, 12, fname);

        try
          frmAdmin.ActionLog('Upload Video success [S] =>'+cmdstr);
          frmAdmin.IdHTTP1.get(cmdstr, MemoryStream);
          Application.ProcessMessages;
          frmAdmin.ShowLog('Finish: Upload Confirmation. ' + flocation + '/'
            + ExtractFileName(txtFileName.Text) + ' (' + fname + ')');
          bSuccess:=true;
        except
          frmAdmin.ActionLog('Upload Video Failed [S] =>'+cmdstr);
          MessageDlg('Error: FTP upload video confirmation failed !!!', mtError, [mbOK], 0);
          frmAdmin.ShowLog('Error!: Upload Confirmation Failed. ' + flocation + '/'
            + ExtractFileName(txtFileName.Text) + ' (' + fname + ')');
          abort;
        end;

      // finish sending ftp confirmation block
      // ================================================================================


      except on E: Exception do
        begin
          frmAdmin.ActionLog('Video Upload Failed');
          FTP.Disconnect;
          Application.ProcessMessages;
          MessageDlg('Error: Cannot connect to server !!!'+chr(13)+E.Message, mtError, [mbok], 0);
          Application.ProcessMessages;
          frmAdmin.ShowLog('Error: Cannot FTP to Server. ' + flocation + '/'
            + ExtractFileName(txtFileName.Text) + ' (' + fname + ')');
          Application.ProcessMessages;
        end;
      end;
    end
    else
    begin
      MessageDlg('Error: Upload request - response failed !!!', mtInformation, [mbOK], 0);
      frmAdmin.ShowLog('Error: Upload Request - response error. Upload file abort. ' + flocation + '/' + ExtractFileName(txtFileName.Text));
    end;
  end;

//  frmAdmin.GetServerQ;
//  application.ProcessMessages;

//  frmAdmin.GetServerActiveQF;
  frmAdmin.GetFileListofAllQ;
  application.ProcessMessages;

  self.Close;
  //Self.Enabled := True;
end;

procedure TfrmFTP.FTPWork(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
  ProgressBar1.Position := AWorkCount;
  Application.ProcessMessages;
end;

procedure TfrmFTP.FTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCountMax: Integer);
begin
  ProgressBar1.Position := 0;
  Application.ProcessMessages;
end;

procedure TfrmFTP.FTPWorkEnd(Sender: TObject; AWorkMode: TWorkMode);
begin
  ProgressBar1.Position := 0;
  Application.ProcessMessages;
end;

end.
