unit fmAdmin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AdvOfficePager, Grids, DBGrids, SMDBGrid, StdCtrls, Buttons, DB,
  ComCtrls, GtroDBDateTimePicker, AdvGlassButton, DBCtrls, Mask, RXCtrls,
  ExtCtrls, jpeg, IdBaseComponent, IdComponent, IdTCPConnection, ADODB, NB30,
  IdTCPClient, IdHTTP, FileCtrl, DateUtils, Math, IdAntiFreezeBase,
  IdAntiFreeze, ComObj, AdvGDIPicture, AdvGroupBox, AdvPanel, AdvEdit,INIFiles;

type
  TfrmAdmin = class(TForm)
    MainPage: TAdvOfficePager;
    tbConfig: TAdvOfficePage;
    tbNews: TAdvOfficePage;
    Image3: TImage;
    RxLabel11: TRxLabel;
    IdHTTP1: TIdHTTP;
    tbQueue: TAdvOfficePage;
    btnClear: TAdvGlassButton;
    IdAntiFreeze1: TIdAntiFreeze;
    tbBanner: TAdvOfficePage;
    FontD: TFontDialog;
    ColorDialog1: TColorDialog;
    btnCompact: TAdvGlassButton;
    btnRegister: TAdvGlassButton;
    imgLogo: TAdvGDIPPicture;
    btnHTTP: TAdvGlassButton;
    AdvOfficePager1: TAdvOfficePager;
    pagLog: TAdvOfficePage;
    pagAdminDebug: TAdvOfficePage;
    Memo3: TMemo;
    Memo1: TMemo;
    Memo2: TMemo;
    AdvGroupBox1: TAdvGroupBox;
    RxLabel1: TRxLabel;
    SMDBGrid3: TSMDBGrid;
    btnCancelBanner: TAdvGlassButton;
    btnEditBanner: TAdvGlassButton;
    btnDelBanner: TAdvGlassButton;
    btnNewBanner: TAdvGlassButton;
    AdvGroupBox2: TAdvGroupBox;
    RxLabel20: TRxLabel;
    SMDBGrid2: TSMDBGrid;
    btnCreateQ: TAdvGlassButton;
    btnDelQ: TAdvGlassButton;
    btnEditQ: TAdvGlassButton;
    btnCancelQ: TAdvGlassButton;
    RxLabel12: TRxLabel;
    DBCheckBox3: TDBCheckBox;
    dbtxtSchName: TDBEdit;
    RxLabel13: TRxLabel;
    d1: TGtroDBDateTimePicker;
    t1: TGtroDBDateTimePicker;
    d2: TGtroDBDateTimePicker;
    t2: TGtroDBDateTimePicker;
    Bevel2: TBevel;
    t4: TGtroDBDateTimePicker;
    RxLabel45: TRxLabel;
    t3: TGtroDBDateTimePicker;
    RxLabel44: TRxLabel;
    RxLabel42: TRxLabel;
    dbchkVideoSat: TDBCheckBox;
    RxLabel41: TRxLabel;
    dbchkVideoFri: TDBCheckBox;
    RxLabel40: TRxLabel;
    dbchkVideoThu: TDBCheckBox;
    RxLabel39: TRxLabel;
    dbchkVideoWed: TDBCheckBox;
    RxLabel38: TRxLabel;
    dbchkVideoTue: TDBCheckBox;
    RxLabel37: TRxLabel;
    dbchkVideoMon: TDBCheckBox;
    RxLabel43: TRxLabel;
    dbchkVideoSun: TDBCheckBox;
    AdvGroupBox3: TAdvGroupBox;
    dbgNews: TSMDBGrid;
    btnCreateNews: TAdvGlassButton;
    btnDelNews: TAdvGlassButton;
    btnEditNews: TAdvGlassButton;
    btnCancelNews: TAdvGlassButton;
    AdvGroupBox4: TAdvGroupBox;
    RxLabel5: TRxLabel;
    DBEdit5: TDBEdit;
    RxLabel6: TRxLabel;
    chkActiveNews: TDBCheckBox;
    RxLabel10: TRxLabel;
    btnFontSet: TAdvGlassButton;
    btnBackColor: TAdvGlassButton;
    Label1: TLabel;
    d1n: TGtroDBDateTimePicker;
    t1n: TGtroDBDateTimePicker;
    Label2: TLabel;
    d2n: TGtroDBDateTimePicker;
    t2n: TGtroDBDateTimePicker;
    RxLabel49: TRxLabel;
    RxLabel50: TRxLabel;
    RxLabel51: TRxLabel;
    AdvGroupBox5: TAdvGroupBox;
    RxLabel21: TRxLabel;
    SMDBGrid1: TSMDBGrid;
    btnCreateF: TAdvGlassButton;
    btnDelF: TAdvGlassButton;
    btnEditF: TAdvGlassButton;
    btnCancelQF: TAdvGlassButton;
    RxLabel14: TRxLabel;
    DBCheckBox4: TDBCheckBox;
    DBEdit8: TDBEdit;
    RxLabel15: TRxLabel;
    AdvGroupBox6: TAdvGroupBox;
    RxLabel19: TRxLabel;
    DBEdit11: TDBEdit;
    RxLabel22: TRxLabel;
    DBEdit12: TDBEdit;
    RxLabel23: TRxLabel;
    DBEdit13: TDBEdit;
    RxLabel24: TRxLabel;
    DBEdit14: TDBEdit;
    RxLabel25: TRxLabel;
    RxLabel4: TRxLabel;
    DBCheckBox2: TDBCheckBox;
    RxLabel3: TRxLabel;
    DBCheckBox1: TDBCheckBox;
    AdvPanel1: TAdvPanel;
    RxLabel16: TRxLabel;
    RxLabel17: TRxLabel;
    RxLabel27: TRxLabel;
    RxLabel34: TRxLabel;
    RxLabel31: TRxLabel;
    RxLabel33: TRxLabel;
    DBComboBox1: TDBComboBox;
    DBEdit1: TDBEdit;
    DBCheckBox6: TDBCheckBox;
    DBEdit10: TDBEdit;
    AdvPanel2: TAdvPanel;
    RxLabel28: TRxLabel;
    RxLabel29: TRxLabel;
    RxLabel30: TRxLabel;
    RxLabel32: TRxLabel;
    RxLabel35: TRxLabel;
    RxLabel47: TRxLabel;
    RxLabel48: TRxLabel;
    RxLabel46: TRxLabel;
    RxLabel52: TRxLabel;
    DBEdit2: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit9: TDBEdit;
    DBCheckBox7: TDBCheckBox;
    DBChkLogoRatio: TDBCheckBox;
    DBChkLogoStretch: TDBCheckBox;
    DBEditLogoWidth: TDBEdit;
    btnSaveConf: TAdvGlassButton;
    btnCancelConf: TAdvGlassButton;
    Timer1: TTimer;
    Memo4: TMemo;
    pagAdminConfig: TAdvOfficePage;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit17: TDBEdit;
    dbDefType: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit21: TDBEdit;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit22: TDBEdit;
    DBCheckBox5: TDBCheckBox;
    RxLabel18: TRxLabel;
    RxLabel53: TRxLabel;
    DBCheckBox9: TDBCheckBox;
    DBEdit7: TDBEdit;
    AdvPanel3: TAdvPanel;
    RxLabel26: TRxLabel;
    edtMAC: TEdit;
    DBEdit16: TDBEdit;
    dbDefFile: TDBEdit;
    cboConfType: TComboBox;
    RxLabel9: TRxLabel;
    txtStream: TEdit;
    lblStream: TRxLabel;
    Label18: TLabel;
    AdvGroupBox7: TAdvGroupBox;
    RxLabel7: TRxLabel;
    RxLabel8: TRxLabel;
    Label5: TLabel;
    Label6: TLabel;
    RxLabel2: TRxLabel;
    chkBannerActive: TDBCheckBox;
    DBComboBox2: TDBComboBox;
    DBComboBox3: TDBComboBox;
    GtroDBDateTimePicker1: TGtroDBDateTimePicker;
    GtroDBDateTimePicker2: TGtroDBDateTimePicker;
    GtroDBDateTimePicker3: TGtroDBDateTimePicker;
    GtroDBDateTimePicker4: TGtroDBDateTimePicker;
    DBEdit18: TDBEdit;
    tbClient: TAdvOfficePage;
    lstClientData: TListBox;
    AdvGroupBox8: TAdvGroupBox;
    lstClient: TListBox;
    AdvGroupBox9: TAdvGroupBox;
    txtClientCode: TAdvEdit;
    txtClientName: TAdvEdit;
    txtClientDesc: TAdvEdit;
    txtClientDB: TAdvEdit;
    btnClientSelect: TAdvGlassButton;
    lblVersion: TLabel;
    tbAdmin: TAdvOfficePage;
    txtAdminPass: TEdit;
    btnAdminLogin: TButton;
    lblAdminPass: TRxLabel;
    frameAdmin: TGroupBox;
    pagAdmin: TAdvOfficePager;
    pagAdminMain: TAdvOfficePage;
    txtRequest: TEdit;
    btnAdminSave: TButton;
    RxLabel36: TRxLabel;
    RxLabel54: TRxLabel;
    btnClientEdit: TAdvGlassButton;
    btnClientCancel: TAdvGlassButton;
    btnClientSave: TAdvGlassButton;
    DBEdit26: TDBEdit;
    pagAdminAction: TAdvOfficePage;
    txtAction: TMemo;
    btnAdminClear: TButton;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCreateNewsClick(Sender: TObject);
    procedure btnEditNewsClick(Sender: TObject);
    procedure btnHTTPClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnCreateQClick(Sender: TObject);
    procedure btnDelQClick(Sender: TObject);
    procedure btnEditQClick(Sender: TObject);
    procedure btnCreateFClick(Sender: TObject);
    procedure btnDelFClick(Sender: TObject);
    procedure btnEditFClick(Sender: TObject);
    procedure btnDelNewsClick(Sender: TObject);
    procedure d1Change(Sender: TObject);
    procedure d2Change(Sender: TObject);
    procedure t1Change(Sender: TObject);
    procedure t2Change(Sender: TObject);
    procedure MainPageChanging(Sender: TObject; FromPage, ToPage: Integer;
      var AllowChange: Boolean);
    procedure d1nChange(Sender: TObject);
    procedure t1nChange(Sender: TObject);
    procedure d2nChange(Sender: TObject);
    procedure t2nChange(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure IdHTTP1Work(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure btnSaveConfClick(Sender: TObject);
    procedure btnCancelConfClick(Sender: TObject);
    procedure btnCancelQClick(Sender: TObject);
    procedure btnCancelQFClick(Sender: TObject);
    procedure btnCancelNewsClick(Sender: TObject);
    procedure btnFontSetClick(Sender: TObject);
    procedure btnNewBannerClick(Sender: TObject);
    procedure btnDelBannerClick(Sender: TObject);
    procedure btnEditBannerClick(Sender: TObject);
    procedure btnCancelBannerClick(Sender: TObject);
    procedure MainPageChange(Sender: TObject);
    procedure btnBackColorClick(Sender: TObject);
    procedure btnCompactClick(Sender: TObject);
    procedure btnRegisterClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cboConfTypeChange(Sender: TObject);
    procedure tbConfigEnter(Sender: TObject);
    procedure btnClientSelectClick(Sender: TObject);
    procedure lstClientClick(Sender: TObject);
    procedure IdHTTP1WorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure btnAdminSaveClick(Sender: TObject);
    procedure btnAdminLoginClick(Sender: TObject);
    procedure btnClientCancelClick(Sender: TObject);
    procedure btnClientEditClick(Sender: TObject);
    procedure btnClientSaveClick(Sender: TObject);
    procedure btnAdminClearClick(Sender: TObject);
    procedure txtActionChange(Sender: TObject);
  private
    { Private declarations }
    procedure DBconnect;


    function CompactAndRepair: Boolean;

  public
    { Public declarations }
    procedure GetServerQ;
    procedure GetServerActiveQF;
    procedure GetServerNews;
    function GenHTTPRequest(forvideo: boolean; cmd: integer; param1: string): string;
    function ExtractResponse(Input: string): TStringList;
    function GenDateTime(input: string): TDateTime;
    function ZeroPad(input: string; cnt: integer): string;
    function ReplaceSpace(input: string): string;

    procedure GetFileListofAllQ;
    procedure GetFileinQFromServer;
    procedure GetServerBanner;

    procedure ShowLog(input: string);
    procedure ActionLog(input: string);

    //function GetAdapterInfo(Lana: Char): String;
    //function GetMACAddress: String;

    procedure GetClientConfig;
    procedure PutClientConfig;

    function GenDOW: string;
    procedure TranslateDOW(input: string);
    function GenIntervalStr(tinput: TDateTime): string;
    function GenTimeInterval(input: string): TDateTime;
    procedure CreateClientList;
  end;
  TVideoQueue = record
    id : integer;
    Name : string;
    SDate,FDate,STime,FTime : TDatetime;
    bSun,bMon,bTue,bWeb,bThu,bFri,bSat,bActive,bRun : boolean;
  end;
const
  cVersion='2.0.4';
var
  frmAdmin: TfrmAdmin;
  Output: TStringList;
  MemoryStream: TMemoryStream;
  ResponseStr: TStringList;

  Client_ID: integer;
  Terminal_ID: integer;
  Client_PWD: string;

  iLabelClick: integer;
  bShowDebug: Boolean;
  Show_News: Boolean;
  Show_Ads: Boolean;
  Video_Dir: string;
  Def_video: string;
  http_server: string;
  video_program: string;
  news_program: string;
  refresh_time: integer;
  ftp_server: string;
  ftp_port: integer;
  ftp_user: string;
  ftp_pass: string;
  ftp_path: string;
  ftp_passive: boolean;
  debug_mode: boolean;
  //MACAddr: string;
  FullVideoDir: string;
  video_source: boolean;
  sProgramDir: string;
  iClientSelect: integer;
  sQueueID: string;

  ClientRun: integer;
  ClientDB: string;

  Registered: boolean;


//  procedure FreePChar(p: PChar); stdcall; external 'dslic.dll';
//  function GetHardDiskSerial: PChar; stdcall; external 'dslic.dll';
//  function IsRegistered: integer; stdcall; external 'dslic.dll';
//  function LicenseOK(Str: string): integer; stdcall; external 'dslic.dll';
//  procedure DoRegister(InputLic: string); stdcall; external 'dslic.dll';

implementation

uses
  data, fmFTP, fmFTP2;

{$R *.dfm}

{
function TfrmAdmin.GetAdapterInfo(Lana: Char): String;
var
  Adapter: TAdapterStatus;
  NCB: TNCB;
begin
  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBRESET);
  NCB.ncb_lana_num := Lana;
  if Netbios(@NCB) <> Char(NRC_GOODRET) then
  begin
    Result := 'No MAC';
    Exit;
  end;

  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBASTAT);
  NCB.ncb_lana_num := Lana;
  NCB.ncb_callname := '*';

  FillChar(Adapter, SizeOf(Adapter), 0);
  NCB.ncb_buffer := @Adapter;
  NCB.ncb_length := SizeOf(Adapter);

  if Netbios(@NCB) <> Char(NRC_GOODRET) then
  begin
    Result := 'No MAC';
    Exit;
  end;

  Result :=
    IntToHex(Byte(Adapter.adapter_address[0]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[1]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[2]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[3]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[4]), 2) + ':' +
    IntToHex(Byte(Adapter.adapter_address[5]), 2);
end;

function TfrmAdmin.GetMACAddress: string;
var
  AdapterList: TLanaEnum;
  NCB: TNCB;
begin
  FillChar(NCB, SizeOf(NCB), 0);
  NCB.ncb_command := Char(NCBENUM);
  NCB.ncb_buffer := @AdapterList;
  NCB.ncb_length := SizeOf(AdapterList);
  Netbios(@NCB);

  if Byte(AdapterList.length) > 0 then
    Result := GetAdapterInfo(AdapterList.lana[0])
  else
    Result := 'No MAC';
end;
}

procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter     := Delimiter;
   ListOfStrings.DelimitedText := Str;
end;

//--------------------------------------------------------------------------------
//  Start and Ending Event of Admin Page
//--------------------------------------------------------------------------------
procedure TfrmAdmin.DBconnect;
begin
  with dm.db do
  begin
    if Connected then Connected := False;
    if (not FileExists(ClientDB)) then
      begin
      ClientDB:='db.mdb';
      end;
    ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0;Jet OLEDB:Database Password=9999;'
                    +'Data Source='+ ClientDB
                    +';Persist Security Info=True';
    Connected := True;
  end;

  with dm do
  begin
    tblConfig.Open;
    tblNews.Open;
    tblQ.Open;
    tblQF.Open;
    tblPlayQ.Open;
    tblBanner.Open;
  end;

  with dm.tblConfig do
  begin
    Client_ID := FieldByName('ClientID').AsInteger;
    Terminal_ID := FieldByName('TerminalID').AsInteger;

    //********************* important parameter ****************************
    Client_PWD := dm.tblConfigsys_pwd.AsString;

    Show_News := FieldByName('show_news').AsBoolean;
    Show_Ads := FieldByName('show_ads').AsBoolean;
    Video_dir := trim(FieldByName('video_directory').AsString);
    Def_video := trim(FieldByName('default_video').AsString);

    http_server := trim(FieldByName('http_server').AsString);
    video_program := trim(FieldByName('video_program').AsString);
    news_program := trim(FieldByName('news_program').AsString);

    refresh_time := FieldByName('refresh_time').AsInteger;
    ftp_server := trim(FieldByName('ftp_server').AsString);
    ftp_port := FieldByName('ftp_port').AsInteger;
    ftp_user := trim(FieldByName('ftp_user').AsString);
    ftp_pass := trim(FieldByName('ftp_pass').AsString);
    ftp_path := trim(FieldByName('ftp_path').AsString);
    ftp_passive := FieldByName('ftp_passive').AsBoolean;
    debug_mode := FieldByName('debug_mode').AsBoolean;
    video_source := FieldByName('video_source').Asboolean;
  end;
end;

procedure TfrmAdmin.FormCreate(Sender: TObject);
var sFile:string;
begin
  Output := TStringList.Create;
  MemoryStream := TMemoryStream.Create;
  ResponseStr := TStringList.Create;

  //MACAddr := GetMACAddress;
  //edtMAC.Text := MACAddr;

//  Registered := IsRegistered <> 0;
  bShowDebug :=false;
  iLabelClick:=0;
  ClientRun:=1;
  ClientDB:='db.01.mdb';
//  btnRegister.Visible := not Registered;
  sProgramDir:=ExtractFilePath(Application.ExeName);
  sFile:=GetCurrentDir+'\media\logo.jpg';
  imgLogo.Visible:=false;
  if (FileExists(sFile)) then
    Image3.Picture.LoadFromFile(sFile)
  Else
    ImgLogo.Visible:=true;
  CreateClientList;
  iClientSelect:=0;
end;

procedure TfrmAdmin.FormDestroy(Sender: TObject);
begin
  //ResponseStr.Free;
  Output.Free;
  MemoryStream.Free;
end;

procedure TfrmAdmin.FormShow(Sender: TObject);
begin
  DBConnect;

  FullVideoDir := ExtractFilePath(Application.ExeName) + Video_Dir;

{
  if FileExists( FullVideoDir + 'logo.bmp' ) then
  begin
    image3.Picture.LoadFromFile(FullVideoDir + 'logo.bmp');
  end;
  if FileExists( FullVideoDir + 'logo.jpg' ) then
  begin
    image3.Picture.LoadFromFile(FullVideoDir + 'logo.jpg');
  end;
}
//  AdminPage.ActivePage := tbConfig;
  MainPage.ActivePage := tbClient;
  DBEdit6.Visible := debug_mode;
  Memo1.Visible := debug_mode;
  Memo2.Visible := debug_mode;
  lblVersion.Caption:='Version '+cVersion;
  //btnHTTP.Visible := debug_mode;
  //Memo3.Visible := debug_mode;
end;

procedure TfrmAdmin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm.tblConfig do
  begin
    if state = dsEdit then post;
  end;

  dm.db.Connected := False;
end;

procedure TfrmAdmin.MainPageChanging(Sender: TObject; FromPage,
  ToPage: Integer; var AllowChange: Boolean);
begin
  if FromPage = 0 then
  begin
    if dm.tblConfig.State in [dsEdit] then
    begin
      AllowChange := False;
      MessageDlg('Configs is in editing, Cannot change page now!', mtError, [mbOK], 0);
    end;
  end;

  if FromPage = 1 then
  begin
    if dm.tblQ.State in [dsEdit, dsInsert] then
    begin
      AllowChange := False;
      MessageDlg('Queue data is in editing, Cannot change page now!', mtError, [mbOK], 0);
    end;

    if dm.tblQF.State in [dsEdit, dsInsert] then
    begin
      AllowChange := False;
      MessageDlg('File List is in Editing, Cannot change page now!', mtError, [mbOK], 0);
    end;
  end;

  if FromPage = 2 then
  begin
    if dm.tblNews.State in [dsEdit, dsInsert] then
    begin
      AllowChange := False;
      MessageDlg('News is in Editing, Cannot change page now!', mtError, [mbOK], 0);
    end;
  end;

  if FromPage = 3 then
  begin
    if dm.tblNews.State in [dsEdit, dsInsert] then
    begin
      AllowChange := False;
      MessageDlg('News is in Editing, Cannot change page now!', mtError, [mbOK], 0);
    end;
  end;
end;

procedure TfrmAdmin.MainPageChange(Sender: TObject);
begin
  case MainPage.ActivePageIndex of
    1: begin
      with dm.tblQ do
      begin
        if active then close;
        open;
      end;

      with dm.tblQF do
      begin
        if active then close;
        open;
      end;
    end;
    2: begin
      with dm.tblNews do
      begin
        if active then close;
        open;
      end;
    end;
    3: begin
      with dm.tblBanner do
      begin
        if active then close;
        open;
      end;
    end;
  end;
end;

procedure TfrmAdmin.ShowLog(input: string);
begin
  Memo3.Lines.Add(FormatDateTime('dd/mm/yyyy hh:nn:ss', now) + ' - ' + input);
end;

procedure TfrmAdmin.ActionLog(input: string);
begin
  txtAction.Lines.Add(FormatDateTime('dd/mm/yyyy hh:nn:ss', now) + ' - ' + input);
end;

procedure TfrmAdmin.btnClearClick(Sender: TObject);
begin
  Memo3.Lines.Clear;
end;

//--------------------------------------------------------------------------------
// Main function for manager data with server
//--------------------------------------------------------------------------------

procedure TfrmAdmin.btnSaveConfClick(Sender: TObject);
begin
  with dm.tblConfig do
  begin
    if State = dsEdit then post;

    Client_id := dm.tblConfigClientID.AsInteger;
    Terminal_id := dm.tblConfigTerminalID.AsInteger;
    Client_PWD := dm.tblConfigsys_pwd.AsString;
    
    PutClientConfig;
  end;
end;

procedure TfrmAdmin.btnCancelConfClick(Sender: TObject);
begin
  with dm.tblConfig do
  begin
    if State = dsEdit then cancel;
  end;
end;

procedure TfrmAdmin.btnHTTPClick(Sender: TObject);
begin
//  ActionLog('GetClientConfig');
  GetClientConfig;
  Application.ProcessMessages;

//  ActionLog('GetServerQ');
  GetServerQ;
  Application.ProcessMessages;

//  ActionLog('GetFileListofAllQ');
  GetFileListofAllQ;
//  GetServerActiveQF;
  Application.ProcessMessages;

//  ActionLog('GetServerNews');
  GetServerNews;
  Application.ProcessMessages;

//  ActionLog('GetServerBanner');
  GetServerBanner;
  Application.ProcessMessages;
end;

procedure TfrmAdmin.GetClientConfig;
var
  i: integer;
  httpstr: string;
begin
  with dm.tblConfig do
  begin
    //if active then close;
    //open;

    if state in [dsEdit, dsInsert] then
      post;

    Client_id := dm.tblConfigClientID.AsInteger;
    Client_PWD := dm.tblConfigsys_pwd.AsString;

    for i := 1 to Fields.Count - 1 do
    begin
      httpstr := GenHTTPRequest(true, 18, inttostr(client_id)
                               + '&attr=' + Fields[i].FieldName);

      try
        Application.ProcessMessages;
        MemoryStream.Clear;
        ActionLog('GetClientConfig [S]=>'+httpstr);
        IdHTTP1.get(httpstr, MemoryStream);
        Application.ProcessMessages;
        MemoryStream.Position := 0;
        Memo2.Lines.LoadFromStream(MemoryStream);
        ActionLog('GetClientConfig [R]=>');
        txtAction.Lines.AddStrings(Memo2.Lines);

        ResponseStr := ExtractResponse(Memo2.Lines[0]);

        if lowercase(ResponseStr.Strings[0]) = lowercase(Fields[i].FieldName) then
        begin
          ShowLog('Finish: Get Configs from Server Client_id = '
                           + inttostr(client_id) + ' ' + Fields[i].FieldName);

          edit;
          if Fields[i].DataType = ftWideString then
          begin
            Fields[i].AsString := ResponseStr.Strings[1];
          end
          else if Fields[i].DataType = ftInteger then
          begin
            try
              Fields[i].AsInteger := strtoint(ResponseStr.Strings[1]);
            except

            end;
          end
          else if Fields[i].DataType = ftBoolean then
          begin
            if LowerCase(ResponseStr.Strings[1]) = 'true' then
              Fields[i].AsBoolean := true
            else
              Fields[i].AsBoolean := false;
          end;
          post;

        end
        else
          ShowLog('Error: Cannot Get Configs from Server Client_id = '
                                 + inttostr(client_id) + ' ' + Fields[i].FieldName);
      except
        ShowLog('Error: Cannot Get Configs from Server Client_id = '
                               + inttostr(client_id) + ' ' + Fields[i].FieldName);
        abort;
      end;

    end; // end of for loop to loop all config fields
    cboConfType.ItemIndex:=strtoint(dbDefType.Text);
    cboConfType.OnChange(cboConfType);
    txtStream.Text:=dbDefFile.Text;
  end;

  with dm.tblConfig do
  begin
    Client_ID := FieldByName('ClientID').AsInteger;
    Terminal_ID := FieldByName('TerminalID').AsInteger;

    //********************* important parameter ****************************
    Client_PWD := dm.tblConfigsys_pwd.AsString;

    Show_News := FieldByName('show_news').AsBoolean;
    Show_Ads := FieldByName('show_ads').AsBoolean;
    Video_dir := trim(FieldByName('video_directory').AsString);
    Def_video := trim(FieldByName('default_video').AsString);

    http_server := trim(FieldByName('http_server').AsString);
    video_program := trim(FieldByName('video_program').AsString);
    news_program := trim(FieldByName('news_program').AsString);

    refresh_time := FieldByName('refresh_time').AsInteger;
    ftp_server := trim(FieldByName('ftp_server').AsString);
    ftp_port := FieldByName('ftp_port').AsInteger;
    ftp_user := trim(FieldByName('ftp_user').AsString);
    ftp_pass := trim(FieldByName('ftp_pass').AsString);
    ftp_path := trim(FieldByName('ftp_path').AsString);
    ftp_passive := FieldByName('ftp_passive').AsBoolean;
    debug_mode := FieldByName('debug_mode').AsBoolean;
    video_source := FieldByName('video_source').Asboolean;
  end;
end;

procedure TfrmAdmin.PutClientConfig;
var
  i: integer;
  httpstr: string;
begin
  with dm.tblConfig do
  begin
    if active then close;
    open;

    for i := 1 to Fields.Count - 1 do
    begin
      httpstr := GenHTTPRequest(true, 17, inttostr(client_id)
                               + '&attr=' + Fields[i].FieldName
                               + '&value=' + Fields[i].AsString);

      try
        Application.ProcessMessages;
        MemoryStream.Clear;
        IdHTTP1.get(httpstr, MemoryStream);
        Application.ProcessMessages;
        ShowLog('Finish: Put Configs to Server Client_id = '
                 + inttostr(client_id) + ' '
                 + Fields[i].FieldName + '=' + Fields[i].AsString)
      except
        ShowLog('Error: Cannot Put Configs to Server Client_id = '
                 + inttostr(client_id) + ' '
                 + Fields[i].FieldName + '=' + Fields[i].AsString);
        abort;
      end;

    end; // end of for loop to loop all config fields
  end;
end;

procedure TfrmAdmin.GetServerQ;
var
  i: integer;
  httpstr:string;
begin
  // get all queue record in server
  try
    Application.ProcessMessages;
    MemoryStream.Clear;
    httpstr:=GenHTTPRequest(true, 0, inttostr(client_id));
    ActionLog('GetServerQ [S]=>'+httpstr);
    IdHTTP1.get(httpstr, MemoryStream);
    Application.ProcessMessages;
    MemoryStream.Position := 0;
    Memo2.Lines.LoadFromStream(MemoryStream);
    ActionLog('GetServerQ [R]=>');
    txtAction.Lines.AddStrings(Memo2.Lines);

    ShowLog('Finish: Get Queue List from Server Client_id = ' + inttostr(client_id));
  except
    ShowLog('Error: Cannot get Queue List from Server Client_id = ' + inttostr(client_id));
    abort;
  end;

    // add all queue from server to local database
  with dm.tblQ do
  begin

//    dm.qryClearQ.ExecSQL;
    dm.qry.Close;
    dm.qry.SQL.Clear;
    dm.qry.SQL.Add('delete from queue;');
    dm.qry.ExecSQL;
    dm.qry.Close;

{
    dm.qry.SQL.Clear;
    dm.qry.SQL.Add('delete from old_queue;');
    dm.qry.ExecSQL;
}
    if dm.tblQ.Active then dm.tblQ.Close;
    dm.tblQ.Open;

{
    if dm.tblOldQ.Active then dm.tblOldQ.Close;
    dm.tblOldQ.Open;
}
    for i := 1 to Memo2.Lines.Count - 1 do
    begin
      try
        ResponseStr := ExtractResponse(Memo2.Lines[i]);

        dm.tblQ.append;
        dm.tblQ.FieldByName('id').AsInteger := strtoint(ResponseStr.Strings[0]);
        dm.tblQ.FieldByName('queue_no').AsString := ResponseStr.Strings[2];
        dm.tblQ.FieldByName('clientid').AsInteger := client_id;
        dm.tblQ.FieldByName('terminalid').AsInteger := terminal_id;

        dm.tblQ.FieldByName('qname').AsString := ResponseStr.Strings[3];
        dm.tblQ.FieldByName('startdate').AsDateTime := GenDateTime(ResponseStr.Strings[4]);
        dm.tblQ.FieldByName('stopdate').AsDateTime := GenDateTime(ResponseStr.Strings[5]);

        dm.tblQ.FieldByName('active').AsBoolean := ResponseStr.Strings[6] = 'Y';

        dm.tblQ.FieldByName('running').AsBoolean := ResponseStr.Strings[7] = 'Y';

        TranslateDOW(ResponseStr.Strings[8]);
        dm.tblQ.FieldByName('startinterval').AsDateTime := GenTimeInterval(ResponseStr.Strings[9]);
        dm.tblQ.FieldByName('endinterval').AsDateTime := GenTimeInterval(ResponseStr.Strings[10]);

        dm.tblQ.post;

{
        dm.tblOldQ.append;

        dm.tblOldQ.FieldByName('id').AsInteger := strtoint(ResponseStr.Strings[0]);
        dm.tblOldQ.FieldByName('queue_no').AsString := ResponseStr.Strings[2];
        dm.tblOldQ.FieldByName('clientid').AsInteger := client_id;
        dm.tblOldQ.FieldByName('terminalid').AsInteger := terminal_id;

        dm.tblOldQ.FieldByName('qname').AsString := ResponseStr.Strings[3];
        dm.tblOldQ.FieldByName('startdate').AsDateTime := GenDateTime(ResponseStr.Strings[4]);
        dm.tblOldQ.FieldByName('stopdate').AsDateTime := GenDateTime(ResponseStr.Strings[5]);

        dm.tblOldQ.FieldByName('active').AsBoolean := ResponseStr.Strings[6] = 'Y';

        dm.tblOldQ.FieldByName('running').AsBoolean := ResponseStr.Strings[7] = 'Y';

        dm.tblOldQ.FieldByName('startinterval').AsDateTime := GenTimeInterval(ResponseStr.Strings[9]);
        dm.tblOldQ.FieldByName('endinterval').AsDateTime := GenTimeInterval(ResponseStr.Strings[10]);

        dm.tblOldQ.post;
}
        ShowLog('Finish: Write Queue to local DB queue no = ' + ResponseStr.Strings[2]);
        Application.ProcessMessages;
      except
        ShowLog('Error: Cannot write Queue to local DB queue no = ' + ResponseStr.Strings[2]);
      end;
    end;
  end;

  with dm.tblQF do
  begin
    if active then close;
    open;
  end;
end;

procedure TfrmAdmin.GetServerActiveQF;
var
  i: integer;
  qno: string;
  sCmd: string;
begin

  with dm.qryGetActiveQ do
  begin
    if active then close;
    open;

    if not Fields[0].IsNull then
      qno := Fields[1].AsString
    else
      qno := '';
  end;

  try
    MemoryStream.Clear;

    if qno <> '' then
    begin
      // get all files from server (both active and inactive queue)
      Application.ProcessMessages;
      sCmd:=GenHTTPRequest(true, 1, qno);
      ActionLog('GetServerActiveQF [S]: '+sCmd);
      IdHTTP1.get(sCmd, MemoryStream);
      Application.ProcessMessages;
      ShowLog('Finish: Get Active File List from Server Queue No. = ' + qno);
    end
    else
    begin
      ShowLog('No active queue in queue list.');
    end;

    MemoryStream.Position := 0;
    Memo2.Lines.LoadFromStream(MemoryStream);
    ActionLog('GetServerActiveQF [R]:');
    txtAction.Lines.AddStrings(Memo2.Lines);

  except
    ShowLog('Error: Cannot get Active File List from Server Queue No. = ' + qno);
    abort;
  end;

  with dm.tblQF do
  begin
    dm.qryclearQF.ExecSQL;
    if Active then Close;
    Open;

    for i := 1 to Memo2.Lines.Count - 1 do
    begin

      try
        ResponseStr := ExtractResponse(Memo2.Lines[i]);
        append;
        FieldByName('id').AsInteger := strtoint(ResponseStr.Strings[0]);
        FieldByName('qid').AsInteger := dm.tblQID.AsInteger; // field to links Q and Q files
        FieldByName('clientid').AsInteger := client_id;
        FieldByName('terminalid').AsInteger := terminal_id;

        FieldByName('filename').AsString := ResponseStr.Strings[1];
        FieldByName('newfilename').AsString := ResponseStr.Strings[7];

        FieldByName('filetype').AsString := ResponseStr.Strings[2];
        FieldByName('filesize').AsInteger := strtoint(ResponseStr.Strings[3]);
        FieldByName('filelocation').AsString := ResponseStr.Strings[4];
        FieldByName('active').AsBoolean := ResponseStr.Strings[5] = 'Y';
        FieldByName('avail').AsBoolean := True;

        if ResponseStr.Strings[6] <> '' then
          FieldByName('ordering').AsInteger := strtoint(ResponseStr.Strings[6])
        else
          FieldByName('ordering').AsInteger := 0;

        post;
        ShowLog('Finish: Write File List to local DB file name = ' + ResponseStr.Strings[1]);
        Application.ProcessMessages;
      except
        ShowLog('Error: Cannot write File List to local DB file name = ' + ResponseStr.Strings[1]);
      end;
    end;
  end;
end;

procedure TfrmAdmin.GetServerNews;
var
  i: integer;
  sCmd: string;
begin
  try
  // get all news record in server
    Application.ProcessMessages;
    MemoryStream.Clear;
    sCmd:=GenHTTPRequest(false, 2, inttostr(client_id));
    ActionLog('GetServerNews [S]: '+sCmd);
    IdHTTP1.get(sCmd, MemoryStream);
    Application.ProcessMessages;
    MemoryStream.Position := 0;
    Memo2.Lines.LoadFromStream(MemoryStream);
    ActionLog('GetServerNews [R]=>');
    txtAction.Lines.AddStrings(Memo2.Lines);
    ShowLog('Finish: Get News List from Server Client_id = ' + inttostr(client_id));
  except
    ShowLog('Error: Cannot get News List from Server Client_id = ' + inttostr(client_id));
    abort;
  end;

  // add all news from server to local database
  with dm.tblNews do
  begin

    dm.qryClearNews.ExecSQL;
    if Active then Close;
    Open;

    for i := 1 to Memo2.Lines.Count - 1 do
    begin
      try
        ResponseStr := ExtractResponse(Memo2.Lines[i]);
        append;
        FieldByName('id').AsInteger := strtoint(ResponseStr.Strings[0]);
        FieldByName('clientid').AsInteger := client_id;
        FieldByName('terminalid').AsInteger := terminal_id;

        FieldByName('headline').AsString := ResponseStr.Strings[2];
        FieldByName('startdate').AsDateTime := GenDateTime(ResponseStr.Strings[4]);
        FieldByName('stopdate').AsDateTime := GenDateTime(ResponseStr.Strings[5]);
        FieldByName('active').AsBoolean := ResponseStr.Strings[6] = 'Y';
        post;
        ShowLog('Finish: Write News to local DB News_id = ' + ResponseStr.Strings[0]);
        Application.ProcessMessages;
      except
        ShowLog('Error: Cannot write News to local DB News_id = ' + ResponseStr.Strings[0]);
      end;
    end;
  end;
end;

procedure TfrmAdmin.GetFileListofAllQ;
begin
  dm.qryclearQF.ExecSQL;
  if dm.tblQf.Active then dm.tblQf.Close;
  dm.tblQf.Open;

  with dm.tblQ do
  begin
    if active then close;
    open;

    first;
    while not eof do
    begin
      GetFileinQFromServer;
      next;
    end;
  end;
end;

procedure TfrmAdmin.GetFileinQFromServer;
var
  i: integer;
  sCmd: string;
begin
  try
    Application.ProcessMessages;
    MemoryStream.Clear;
    sCmd:=GenHTTPRequest(true, 1, dm.tblQ.fieldbyname('queue_no').AsString);
    ActionLog('GetFileinQFromServer [S]: '+sCmd);
    IdHTTP1.get(sCmd, MemoryStream);
    Application.ProcessMessages;
    MemoryStream.Position := 0;
    Memo2.Lines.LoadFromStream(MemoryStream);
    ActionLog('GetFileinQFromServer [R]=> ');
    txtAction.Lines.AddStrings(Memo2.Lines);

  except
    ShowLog('Error: Cannot get File List of Queue No. = ' + dm.tblQ.fieldbyname('queue_no').AsString);
    exit;
  end;

  for i := 1 to Memo2.Lines.Count - 1 do
  begin
    ResponseStr := ExtractResponse(Memo2.Lines[i]);

    with dm.tblQF do
    begin
      try
        ResponseStr := ExtractResponse(Memo2.Lines[i]);
        append;
        FieldByName('id').AsInteger := strtoint(ResponseStr.Strings[0]);
        FieldByName('qid').AsInteger := dm.tblQID.AsInteger; // field to links Q and Q files
        FieldByName('clientid').AsInteger := client_id;
        FieldByName('terminalid').AsInteger := terminal_id;

        FieldByName('filename').AsString := ResponseStr.Strings[1];
        FieldByName('newfilename').AsString := ResponseStr.Strings[7];

        FieldByName('filetype').AsString := ResponseStr.Strings[2];
        FieldByName('filesize').AsInteger := strtoint(ResponseStr.Strings[3]);
        FieldByName('filelocation').AsString := ResponseStr.Strings[4];
        FieldByName('active').AsBoolean := ResponseStr.Strings[5] = 'Y';
        FieldByName('avail').AsBoolean := True;

        if ResponseStr.Strings[6] <> '' then
          FieldByName('ordering').AsInteger := strtoint(ResponseStr.Strings[6])
        else
          FieldByName('ordering').AsInteger := 0;

        post;
        ShowLog('Finish: Write File List to local DB file name = ' + ResponseStr.Strings[1]);
        Application.ProcessMessages;
      except
        ShowLog('Error: Cannot write File List to local DB file name = ' + ResponseStr.Strings[1]);
      end;
    end;
  end;
end;

procedure TfrmAdmin.GetServerBanner;
var
  i: integer;
  sCmd: string;
begin
  try
  // get all banner from server
    Application.ProcessMessages;
    MemoryStream.Clear;
    sCmd:=GenHTTPRequest(false, 13, inttostr(client_id));
    ActionLog('GetServerBanner [S]: '+sCmd);
    IdHTTP1.get(sCmd, MemoryStream);
    Application.ProcessMessages;
    MemoryStream.Position := 0;
    Memo2.Lines.LoadFromStream(MemoryStream);
    ActionLog('GetServerBanner [R]=> ');
    txtAction.Lines.AddStrings(Memo2.Lines);

    ShowLog('Finish: Get Banner from Server Client_id = ' + inttostr(client_id));
  except
    ShowLog('Error: Cannot get Banner from Server Client_id = ' + inttostr(client_id));
    abort;
  end;

  // add all banner from server to local database
  with dm.tblBanner do
  begin

    dm.qryClearBanner.ExecSQL;
    if Active then Close;
    Open;

    for i := 1 to Memo2.Lines.Count - 1 do
    begin
      try
        ResponseStr := ExtractResponse(Memo2.Lines[i]);
        append;
        FieldByName('id').AsInteger := strtoint(ResponseStr.Strings[0]);
        FieldByName('clientid').AsInteger := client_id;
        FieldByName('terminalid').AsInteger := terminal_id;

        FieldByName('filename').AsString := ResponseStr.Strings[1];
        FieldByName('newfilename').AsString := ResponseStr.Strings[7];

        FieldByName('filelocation').AsString := ResponseStr.Strings[8];

        FieldByName('showposition').AsInteger := strtoint(ResponseStr.Strings[2]);
        FieldByName('showtype').AsInteger := strtoint(ResponseStr.Strings[3]);

        FieldByName('startdate').AsDateTime := GenDateTime(ResponseStr.Strings[4]);
        FieldByName('stopdate').AsDateTime := GenDateTime(ResponseStr.Strings[5]);
        FieldByName('active').AsBoolean := ResponseStr.Strings[6] = 'Y';
        post;
        ShowLog('Finish: Write Banner to local DB id = ' + ResponseStr.Strings[0]);
        Application.ProcessMessages;
      except
        ShowLog('Error: Cannot write Banner to local DB id = ' + ResponseStr.Strings[0]);
      end;
    end;
  end;
end;

function TfrmAdmin.ExtractResponse(Input: string): TStringList;
var
  i: integer;
  Temp: string;

begin
  Output.Clear;
  Temp := '';

  for i := 1 to Length(Input) do
  begin
    if (Input[i] <> '|') and (i <> Length(Input)) then
    begin
      Temp := Temp + Input[i];
    end
    else
    begin
      if (Trim(Input[i]) <> '') and (Input[i] <> '|') then
        Temp := Temp + Input[i];
      Output.Add(Trim(Temp));
      Temp := '';
    end;
  end;

  result := Output;
end;

function TfrmAdmin.GenHTTPRequest(forvideo: boolean; cmd: integer; param1: string): string;
begin
  if forvideo then
    result := http_server + video_program + memo1.Lines[cmd]
  else
    result := http_server + news_program + memo1.Lines[cmd];
                //+ '&mac=' + MACAddr;

  result:=result+ param1
                + '&ver=' + cVersion
                + '&sys_pwd=' + Client_PWD
                + '&terminalid=' + inttostr(Terminal_ID)

end;

function TfrmAdmin.GenDateTime(input: string): TDateTime;
var
  yy, mm, dd, hh, min, ss: word;
  sinput: string;
begin
  sinput :=  trim(input);
  if length(sinput) = 19 then
  begin
    try
      yy := strtoint( sinput[1] + sinput[2] + sinput[3] + sinput[4] );
      mm := strtoint( sinput[6] + sinput[7] );
      dd := strtoint( sinput[9] + sinput[10] );

      hh :=  strtoint( sinput[12] + sinput[13] );
      min := strtoint( sinput[15] + sinput[16] );
      ss :=  strtoint( sinput[18] + sinput[19] );

      Result := EncodeDateTime(yy, mm, dd, hh, min, ss, 0);
    except
      result := Now;
    end;
  end
  else
    result := now;
end;

//--------------------------------------------------------------------------------
// procedure for Queue and Queue File Management
//--------------------------------------------------------------------------------
procedure TfrmAdmin.btnCreateQClick(Sender: TObject);
begin
  with dm.tblQ do
  begin
    if state in [dsEdit, dsInsert] then abort;

    if not (state in [dsEdit, dsInsert]) then
    begin

      //d1.DateTime := Now;
      //t1.DateTime := Now;
      //d2.DateTime := Now;
      //t2.DateTime := Now;
      try
        Append;
// Add By Boripat
        d1.DateTime := date;
        d2.DateTime := Tomorrow;
        t1.DateTime:=date;
        t2.DateTime:=Tomorrow;

        t3.DateTime:=date;
//        t3.Time:=time;
        t3.Time:=strToTime('00:00:01');
        t4.DateTime:=date;
//        t4.Time:=time;
        t4.Time:=StrToTime('23:59:59');
// End Add by Boripat


// Comment by Boripat
{
        FieldByName('StartDate').AsDateTime := Now;
        FieldByName('StopDate').AsDateTime := Now;

        FieldByName('StartInterval').AsDateTime := Now;
        FieldByName('EndInterval').AsDateTime := Now;

        FieldByName('active').AsBoolean := false;
}
// Add by Boripat

        FieldByName('StartDate').AsDateTime := d1.DateTime;
        FieldByName('StopDate').AsDateTime := d2.DateTime;

        FieldByName('StartInterval').AsDateTime := t3.DateTime;
        FieldByName('EndInterval').AsDateTime := t4.DateTime;

        FieldByName('active').AsBoolean := false;

        FieldByName('IsSunday').AsBoolean := true;
        FieldByName('IsMonday').AsBoolean := true;
        FieldByName('IsTuesday').AsBoolean := true;
        FieldByName('IsWednesday').AsBoolean := true;
        FieldByName('IsThursday').AsBoolean := true;
        FieldByName('IsFriday').AsBoolean := true;
        FieldByName('IsSaturday').AsBoolean := true;
        dbtxtSchName.SetFocus;
//
      except
        cancel;
//        MessageDlg('Cannot Add New Queue.', mtInformation, [mbOK], 0);
      end;

    end;
  end;
end;

// manage user control to select date and time
procedure TfrmAdmin.d1Change(Sender: TObject);
begin
  t1.DateTime := int(d1.Date) + frac(t1.Time);
end;

procedure TfrmAdmin.d2Change(Sender: TObject);
begin
  t2.DateTime := int(d2.Date) + frac(t2.Time);
end;

procedure TfrmAdmin.t1Change(Sender: TObject);
begin
  d1.DateTime := int(d1.Date) + frac(t1.Time);
end;

procedure TfrmAdmin.t2Change(Sender: TObject);
begin
  d2.DateTime := int(d2.Date) + frac(t2.Time);
end;

procedure TfrmAdmin.btnDelQClick(Sender: TObject);
var
  cmdstr: string;
begin
  if MessageDlg('Delete queue confirmation !!!', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    if not (dm.tblQ.State in [dsEdit, dsInsert]) then
    begin
      MemoryStream.Clear;

      cmdstr := GenHTTPRequest(true, 5, dm.tblQ.fieldbyname('id').AsString);

      try
        ActionLog('Delete Queue [S]: '+cmdstr);
        IdHTTP1.get(cmdstr, MemoryStream);
        MemoryStream.Position := 0;
        Memo2.Lines.Clear;
        Memo2.Lines.LoadFromStream(MemoryStream);
        ActionLog('Delete Queue [R]=>');
        txtAction.Lines.AddStrings(Memo2.Lines);
        with dm.tblQ do
        begin
          if not IsEmpty then
            delete; 
        end;
      except
        MessageDlg('Error: Cannot connect to server!!!', mtError, [mbOK], 0);
        ShowLog('Error: Cannot Delete Queue id = ' + dm.tblQ.fieldbyname('id').AsString);
        btnHTTP.Click;
        abort;
      end;
    end;

    //btnHTTP.Click;
  end;
end;

procedure TfrmAdmin.btnEditQClick(Sender: TObject);
var
  activestr: string;
  cmdstr: string;
  AlreadyHaveActiveQ: boolean;
  sSQL: string;
  myQ: TVideoQueue;
  sStart,sStop : string;
  begin
  {
  with dm.qryAnotherActiveQ do
  begin
    if active then close;
    Parameters.ParamByName('qid').Value := dm.tblQ.fieldbyname('id').AsInteger;
    open;
    AlreadyHaveActiveQ := not Fields[0].IsNull;
  end;
  }

  Selectnext(ActiveControl, true, true);

//
  {
  if (CheckDupPeriod()) then
    begin
    abort;
    end
  Else
    begin
    Restore();
    end;
}

  with dm.tblQ do
  begin
    if not (state in [dsedit, dsinsert]) then abort;
//
    sStart:=FormatDateTime('hh:nn',t1.Time);
    sStop:=FormatDateTime('hh:nn',t2.Time);
    t1.Time:=strtotime(sStart);
    t2.Time:=strtotime(sStop);
    fieldbyname('startdate').AsDateTime := int(d1.Date) + frac(t1.Time);
    fieldbyname('stopdate').AsDateTime := int(d2.Date) + frac(t2.Time);

    sStart:=FormatDateTime('hh:nn',t3.Time);
    sStop:=FormatDateTime('hh:nn',t4.Time);
    t3.Time:=strtotime(sStart);
    t4.Time:=strtotime(sStop);

    if FieldByName('qname').AsString = '' then
    begin
      MessageDlg('Error: Queue name must be entered !!!', mtError, [mbOK], 0);
      abort;
    end;

    if FieldByName('stopdate').AsDateTime <= Now then
    begin
      MessageDlg('Error: Stop date must be the future time !!!', mtError, [mbOK], 0);
      abort;
    end;

    if FieldByName('stopdate').AsDateTime <= FieldByName('startdate').AsDateTime then
    begin
      MessageDlg('Error: Stop date must be later than start date !!!', mtError, [mbOK], 0);
      abort;
    end;

    if frac(FieldByName('endinterval').AsDateTime) <=
       frac(FieldByName('startinterval').AsDateTime) then
    begin
      MessageDlg('Error: Stop Interval must beyond start interval !!!', mtError, [mbOK], 0);
      abort;
    end;

// Add by Boripat
{
    if (dm.tblQ.RecordCount > 1) then
    begin
      if (dm.qryDupQueue.Active) then dm.qryDupQueue.Close;
      sSQL:=dm.qryDupQueue.SQL.Text;
      sSQL:=stringReplace(sSQL,'[STIME]',FormatDateTime('#hh.nn.00#',FieldByName('startinterval').AsDateTime), [rfReplaceAll, rfIgnoreCase]);
      sSQL:=stringReplace(sSQL,'[FTIME]',FormatDateTime('#hh.nn.00#',FieldByName('endinterval').AsDateTime), [rfReplaceAll, rfIgnoreCase]);
      sSQL:=stringReplace(sSQL,'[SDATE]',FormatDateTime('#yyyy-mm-dd hh.nn.00#',FieldByName('startdate').AsDateTime), [rfReplaceAll, rfIgnoreCase]);
      sSQL:=stringReplace(sSQL,'[FDATE]',FormatDateTime('#yyyy-mm-dd hh.nn.00#',FieldByName('stopdate').AsDateTime), [rfReplaceAll, rfIgnoreCase]);
      sSQL:=stringReplace(sSQL,'[bSun]',FieldByName('IsSunday').asstring, [rfReplaceAll, rfIgnoreCase]);
      sSQL:=stringReplace(sSQL,'[bMon]',FieldByName('IsMonday').asstring, [rfReplaceAll, rfIgnoreCase]);
      sSQL:=stringReplace(sSQL,'[bTue]',FieldByName('IsTuesday').asstring, [rfReplaceAll, rfIgnoreCase]);
      sSQL:=stringReplace(sSQL,'[bWed]',FieldByName('IsWednesday').asstring, [rfReplaceAll, rfIgnoreCase]);
      sSQL:=stringReplace(sSQL,'[bThu]',FieldByName('IsThursday').asstring, [rfReplaceAll, rfIgnoreCase]);
      sSQL:=stringReplace(sSQL,'[bFri]',FieldByName('IsFriday').asstring, [rfReplaceAll, rfIgnoreCase]);
      sSQL:=stringReplace(sSQL,'[bSat]',FieldByName('IsSaturday').asstring, [rfReplaceAll, rfIgnoreCase]);
      if (state = dsinsert) then
        begin
        sSQL:=stringReplace(sSQL,'[myID]','0', [rfReplaceAll, rfIgnoreCase]);
        end
      Else
        begin
        sSQL:=stringReplace(sSQL,'[myID]',FieldByName('id').asstring, [rfReplaceAll, rfIgnoreCase]);
        end;

      sSQL:=stringReplace(sSQL,''#$D#$A'',' ', [rfReplaceAll, rfIgnoreCase]);


//    MessageDlg('SQL :' +sSQL, mtError, [mbOK], 0);

      dm.qry.Close;
      dm.qry.SQL.Clear;
      dm.qry.SQL.add(sSQL);
      Memo4.Clear;
      Memo4.Lines.Text:=sSQL;

      dm.qry.open;
  //    dm.qry.ExecSQL;
      if (dm.qry.RecordCount > 0) then
        begin
        MessageDlg('Error: Your selected queue is not available, Please check time overlap with other queue !!!', mtError, [mbOK], 0);
        abort;
        end;
      dm.qry.close;
    end;
}
// End Add by Boripat
    {
    if AlreadyHaveActiveQ and fieldbyname('active').AsBoolean then
    begin
      MessageDlg('Error: There is an Active Queue already. Only one active queue is allowed !!!', mtError, [mbOK], 0);
      abort;
    end;
    }

    if state = dsinsert then
    begin
      MemoryStream.Clear;

      if fieldbyname('active').AsBoolean then
        activestr := 'Y'
      else
        activestr := 'N';

      cmdstr := GenHTTPRequest(true, 3,
                      inttostr(client_id) + '&name=' + ReplaceSpace(fieldbyname('qname').AsString)
                    + '&startdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('startdate').AsDateTime)
                    + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('startdate').AsDateTime)
                    + '&stopdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('stopdate').AsDateTime)
                    + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('stopdate').AsDateTime)
                    + '&dow=' + GenDOW
                    + '&startinterval=' + GenIntervalStr(fieldbyname('startinterval').AsDateTime)
                    + '&endinterval=' + GenIntervalStr(fieldbyname('endinterval').AsDateTime)
                    + '&active=' + activestr);

      //showmessage(cmdstr);

      try
        ActionLog('Insert Queue [S]: '+cmdstr);
        IdHTTP1.get(cmdstr, MemoryStream);
        MemoryStream.Position := 0;
        Memo2.Lines.Clear;
        Memo2.Lines.LoadFromStream(MemoryStream);
        ActionLog('Insert Queue [R]=>');
        txtAction.Lines.AddStrings(Memo2.Lines);
        //showmessage(Memo2.Lines.CommaText);
      except
        MessageDlg('Error: Cannot connect to server !!!', mtError, [mbOK], 0);
        ShowLog('Error: Cannot insert Queue to Server ');
        cancel;
        abort;
      end;

      cancel;
      //GetServerQ;

    end
    else if state = dsedit then
    begin
      MemoryStream.Clear;

      if fieldbyname('active').AsBoolean then
        activestr := 'Y'
      else
        activestr := 'N';

      cmdstr := GenHTTPRequest(true, 4,
                      fieldbyname('id').AsString + '&name=' + ReplaceSpace(fieldbyname('qname').AsString)
                    + '&startdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('startdate').AsDateTime)
                    + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('startdate').AsDateTime)
                    + '&stopdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('stopdate').AsDateTime)
                    + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('stopdate').AsDateTime)
                    + '&dow=' + GenDOW
                    + '&startinterval=' + GenIntervalStr(fieldbyname('startinterval').AsDateTime)
                    + '&endinterval=' + GenIntervalStr(fieldbyname('endinterval').AsDateTime)
                    + '&active=' + activestr);

      try
        ActionLog('Edit Queue [S]: '+cmdstr);
        IdHTTP1.get(cmdstr, MemoryStream);
        MemoryStream.Position := 0;
        Memo2.Lines.Clear;
        Memo2.Lines.LoadFromStream(MemoryStream);
        ActionLog('Edit Queue [R]=>');
        txtAction.Lines.AddStrings(Memo2.Lines);
      except
        MessageDlg('Error: Cannot connect to server !!!', mtError, [mbOK], 0);
        ShowLog('Error: Cannot Edit Queue in Server');
        Cancel;
        abort;
      end;

      cancel;
      //GetServerQ;

    end;
  end;

  btnHTTP.Click;
end;

procedure TfrmAdmin.btnCancelQClick(Sender: TObject);
begin
  if dm.tblQ.State in [dsEdit, dsInsert] then
  begin
    dm.tblQ.Cancel;
    if (dm.tblQ.RecordCount < 1) then
      begin
      d1.DateTime:=Time;
      d2.DateTime:=Time;
      t1.DateTime:=Time;
      t2.DateTime:=Time;
      t3.DateTime:=Time;
      t4.DateTime:=Time;

      end;
  end;
end;

procedure TfrmAdmin.btnCreateFClick(Sender: TObject);
begin
  sQueueID:=dm.dscQ.DataSet.FieldValues['id'];
  frmFTP.ShowModal;
end;

procedure TfrmAdmin.btnDelFClick(Sender: TObject);
var
  cmdstr: string;
begin
  if MessageDlg('Delete Video confirmation !!!', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    MemoryStream.Clear;

    cmdstr := GenHTTPRequest(true, 8, dm.tblQF.fieldbyname('id').AsString);

    try
      ActionLog('Delete Video [S]: '+cmdStr);
      IdHTTP1.get(cmdstr, MemoryStream);
      MemoryStream.Position := 0;
      Memo2.Lines.Clear;
      Memo2.Lines.LoadFromStream(MemoryStream);
      ActionLog('Delete Video [R]=>');
      txtAction.Lines.AddStrings(Memo2.Lines);
    except
      MessageDlg('Error: Cannot connect to server !!!', mtError, [mbOK], 0);
      ShowLog('Error: Cannot delete Video name = ' + dm.tblQF.fieldbyname('filename').AsString);
      abort;
    end;

    if strtoint(Memo2.Lines[0]) > 0 then
    begin
      dm.tblQF.Delete;
    end;
  end;
end;

procedure TfrmAdmin.btnEditFClick(Sender: TObject);
var
  activestr: string;
  cmdstr: string;
begin
  Selectnext(ActiveControl, true, true);

  with dm.tblQF do
  begin
    if not (state in [dsedit, dsinsert]) then abort;

    if state = dsEdit then
    begin
      MemoryStream.Clear;

      if fieldbyname('active').AsBoolean then
        activestr := 'Y'
      else
        activestr := 'N';

      cmdstr := GenHTTPRequest(true, 7,
                      fieldbyname('id').AsString
                    + '&ordering=' + fieldbyname('ordering').AsString
                    + '&active=' + activestr);

      try
        ActionLog('Edit Video [S]: '+cmdStr);
        IdHTTP1.get(cmdstr, MemoryStream);
        MemoryStream.Position := 0;
        Memo2.Lines.Clear;
        Memo2.Lines.LoadFromStream(MemoryStream);
        ActionLog('Edit Video [R]=>');
        txtAction.Lines.AddStrings(Memo2.Lines);
      except
        MessageDlg('Error: Cannot connect to server !!!', mtError, [mbOK], 0);
        ShowLog('Error: Cannot edit Video name = ' + fieldbyname('filename').AsString);
        cancel;
        abort;
      end;

      if strtoint(Memo2.Lines[0]) > 0 then
      begin
        Post;
      end
      else
      begin
        Cancel;
      end;
    end;

    if active then close;
    open;
  end;
end;

procedure TfrmAdmin.btnCancelQFClick(Sender: TObject);
begin
  if dm.tblQF.State in [dsEdit, dsInsert] then
  begin
    dm.tblQF.Cancel;
  end;
end;

//--------------------------------------------------------------------------------
// procedure for News Management
//--------------------------------------------------------------------------------
procedure TfrmAdmin.btnCreateNewsClick(Sender: TObject);
begin
  with dm.tblNews do
  begin
    if not (state in [dsEdit, dsInsert]) then
    begin
      try
        Append;
        FieldByName('StartDate').AsDateTime := Now;
        FieldByName('StopDate').AsDateTime := Now;
        FieldByName('active').AsBoolean := True;

        d1n.DateTime := Now;
        t1n.DateTime := Now;
        d2n.DateTime := Now;
        t2n.DateTime := Now;
      except

      end;

    end;
  end;
end;

procedure TfrmAdmin.d1nChange(Sender: TObject);
begin
  t1n.DateTime := int(d1n.Date) + frac(t1n.Time);
end;

procedure TfrmAdmin.t1nChange(Sender: TObject);
begin
  d1n.DateTime := int(d1n.Date) + frac(t1n.Time);
end;

procedure TfrmAdmin.d2nChange(Sender: TObject);
begin
  t2n.DateTime := int(d2n.Date) + frac(t2n.Time);
end;

procedure TfrmAdmin.t2nChange(Sender: TObject);
begin
  d2n.DateTime := int(d2n.Date) + frac(t2n.Time);
end;

procedure TfrmAdmin.btnDelNewsClick(Sender: TObject);
var
  cmdstr: string;
begin
  if MessageDlg('Delete News confirmation !!!', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    if not (dm.tblNews.State in [dsEdit, dsInsert]) then
    begin
      MemoryStream.Clear;

      cmdstr := GenHTTPRequest(false, 11, dm.tblNews.fieldbyname('id').AsString);

      try
        ActionLog('Delete News [S]: '+cmdStr);
        IdHTTP1.get(cmdstr, MemoryStream);
        MemoryStream.Position := 0;
        Memo2.Lines.Clear;
        Memo2.Lines.LoadFromStream(MemoryStream);
        ActionLog('Delete News [R]=>');
        txtAction.Lines.AddStrings(Memo2.Lines);
      except
        MessageDlg('Error: Cannot connect to server !!!', mtError, [mbOK], 0);
        ShowLog('Error: Cannot delete News id = ' + dm.tblNews.fieldbyname('id').AsString);
        GetServerNews;
        abort;
      end;

      GetServerNews;
    end;
  end;
end;

procedure TfrmAdmin.btnEditNewsClick(Sender: TObject);
var
  activestr: string;
  cmdstr: string;
begin
  Selectnext(ActiveControl, true, true);

  with dm.tblNews do
  begin
    if not (state in [dsedit, dsinsert]) then abort;

    fieldbyname('startdate').AsDateTime := int(d1n.Date) + frac(t1n.Time);
    fieldbyname('stopdate').AsDateTime := int(d2n.Date) + frac(t2n.Time);

    if FieldByName('headline').AsString = '' then
    begin
      MessageDlg('EError: Head line must be entered !!!', mtError, [mbOK], 0);
      abort;
    end;

    if FieldByName('stopdate').AsDateTime <= Now then
    begin
      MessageDlg('Error: Stop date must be the future time !!!', mtError, [mbOK], 0);
      abort;
    end;

    if FieldByName('stopdate').AsDateTime <= FieldByName('startdate').AsDateTime then
    begin
      MessageDlg('Error: Stop date must be later than start date !!!', mtError, [mbOK], 0);
      abort;
    end;

    if state = dsinsert then
    begin
      MemoryStream.Clear;

      if fieldbyname('active').AsBoolean then
        activestr := 'Y'
      else
        activestr := 'N';

      cmdstr := GenHTTPRequest(false, 9,
                      inttostr(client_id)
                    + '&headline=' + ReplaceSpace( fieldbyname('headline').AsString )
                    + '&startdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('startdate').AsDateTime)
                    + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('startdate').AsDateTime)
                    + '&stopdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('stopdate').AsDateTime)
                    + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('stopdate').AsDateTime)
                    + '&active=' + activestr);

      try
        ActionLog('Insert News [S]: '+cmdStr);
        IdHTTP1.get(cmdstr, MemoryStream);
        MemoryStream.Position := 0;
        Memo2.Lines.Clear;
        Memo2.Lines.LoadFromStream(MemoryStream);
        ActionLog('Insert News [R]=>');
        txtAction.Lines.AddStrings(Memo2.Lines);
      except
        MessageDlg('Error: Cannot connect to server !!!', mtError, [mbOK], 0);
        ShowLog('Error: Cannot insert News to Server.');
        Cancel;
        abort;
      end;

      Cancel;
      GetServerNews;

    end
    else if state = dsedit then
    begin
      MemoryStream.Clear;

      if fieldbyname('active').AsBoolean then
        activestr := 'Y'
      else
        activestr := 'N';

      cmdstr := GenHTTPRequest(false, 10,
                      fieldbyname('id').AsString
                    + '&headline=' + ReplaceSpace( fieldbyname('headline').AsString )
                    + '&startdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('startdate').AsDateTime)
                    + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('startdate').AsDateTime)
                    + '&stopdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('stopdate').AsDateTime)
                    + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('stopdate').AsDateTime)
                    + '&active=' + activestr);

      try
        ActionLog('Edit News [S]: '+cmdStr);
        IdHTTP1.get(cmdstr, MemoryStream);
        MemoryStream.Position := 0;
        Memo2.Lines.Clear;
        Memo2.Lines.LoadFromStream(MemoryStream);
        ActionLog('Edit News [R]=>');
        txtAction.Lines.AddStrings(Memo2.Lines);
      except
        MessageDlg('EError: Cannot connect to server !!!', mtError, [mbOK], 0);
        ShowLog('Error: Cannot edit News id = ' + fieldbyname('id').AsString);
        Cancel;
        abort;
      end;

      Cancel;
      GetServerNews;

    end;

  end;
end;

procedure TfrmAdmin.btnCancelNewsClick(Sender: TObject);
begin
  if dm.tblNews.State in [dsEdit, dsInsert] then
  begin
    dm.tblNews.Cancel;
  end;
end;

function TfrmAdmin.ZeroPad(input: string; cnt: integer): string;
var
  i: integer;
begin
  result := input;
  for i := length(input) to cnt - 1 do
  begin
    result := '0' + result;
  end;
end;

function TfrmAdmin.ReplaceSpace(input: string): string;
var
  i: integer;
begin
  result := '';
  for i := 1 to length(input) do
  begin
    if input[i] = ' ' then
    begin
      result := result + '%20'
    end
    else
    begin
      result := result + input[i]
    end;
  end;
end;

procedure TfrmAdmin.IdHTTP1Work(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
  Application.ProcessMessages;
end;

procedure TfrmAdmin.btnFontSetClick(Sender: TObject);
var
  FontTemp: TFontStyles;
begin
  with dm.tblConfig do
  begin
    if FieldByName('news_font').AsString <> '' then
      FontD.Font.Name := FieldByName('news_font').AsString
    else
      FontD.Font.Name := 'Tahoma';

    if FieldByName('news_size').AsString <> '' then
      FontD.Font.Size := FieldByName('news_size').AsInteger
    else
      FontD.Font.Size := 24;

    if FieldByName('news_color').AsString <> '' then
      FontD.Font.Color := FieldByName('news_color').Value
    else
      FontD.Font.Color := clWhite;

    FontTemp := [];
    if FieldByName('news_isbold').AsInteger <> 0 then
      Include(FontTemp, fsBold);
    if FieldByName('news_isitalic').AsInteger <> 0 then
      Include(FontTemp, fsItalic);
    if FieldByName('news_isunderline').AsInteger <> 0 then
      Include(FontTemp, fsUnderline);
    FontD.Font.Style := FontTemp;
  end;

  if FontD.Execute then
  begin
    with dm.tblConfig do
    begin
      edit;
      FieldByName('news_font').AsString := FontD.Font.Name;
      FieldByName('news_size').AsInteger := FontD.Font.Size;
      FieldByName('news_color').AsInteger := FontD.Font.Color;

      if fsBold in FontD.font.Style then
        FieldByName('news_isbold').AsInteger := 1
      else
        FieldByName('news_isbold').AsInteger := 0;
      if fsItalic in FontD.font.Style then
        FieldByName('news_isitalic').AsInteger := 1
      else
        FieldByName('news_isitalic').AsInteger := 0;
      if fsUnderline in FontD.font.Style then
        FieldByName('news_isunderline').AsInteger := 1
      else
        FieldByName('news_isunderline').AsInteger := 0;

      post;
    end;

    btnSaveConf.Click;
  end;
end;

procedure TfrmAdmin.btnNewBannerClick(Sender: TObject);
begin
  frmFTP2.ShowModal;
end;

procedure TfrmAdmin.btnDelBannerClick(Sender: TObject);
var
  cmdstr: string;
begin
  if MessageDlg('Delete Banner Confirmation !!!', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    MemoryStream.Clear;

    cmdstr := GenHTTPRequest(true, 16, dm.tblBanner.fieldbyname('id').AsString);

    try
      ActionLog('Delete News [S]: '+cmdStr);
      IdHTTP1.get(cmdstr, MemoryStream);
      MemoryStream.Position := 0;
      Memo2.Lines.Clear;
      Memo2.Lines.LoadFromStream(MemoryStream);
      ActionLog('Delete News [R]=>');
      txtAction.Lines.AddStrings(Memo2.Lines);
    except
      MessageDlg('Error: Cannot connect to server !!!', mtError, [mbOK], 0);
      ShowLog('Error: Cannot delete Banner name = ' + dm.tblBanner.fieldbyname('filename').AsString);
      btnHTTP.Click;
      abort;
    end;

    if strtoint(Memo2.Lines[0]) > 0 then
    begin
      dm.tblBanner.Delete;
    end;

    btnHTTP.Click;
  end;
end;

procedure TfrmAdmin.btnEditBannerClick(Sender: TObject);
var activestr,cmdstr: string;
begin
  with dm.tblBanner do
    if State in [dsEdit, dsInsert] then
    begin
      MemoryStream.Clear;

//      if fieldbyname('active').AsBoolean then
      if (chkBannerActive.Checked) then
        activestr := 'Y'
      else
        activestr := 'N';

      cmdstr := GenHTTPRequest(true, 20, inttostr(client_id)
            + '&id='+ inttostr(fieldbyname('id').AsInteger)
            + '&position=' + inttostr(fieldbyname('ShowPosition').asInteger)
            + '&type=' + inttostr(fieldbyname('ShowType').asInteger)
            + '&startdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('startdate').AsDateTime)
            + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('startdate').AsDateTime)
            + '&stopdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('stopdate').AsDateTime)
            + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('stopdate').AsDateTime)
            + '&active=' + activestr);

      try
        ActionLog('Edit News [S]: '+cmdStr);
        IdHTTP1.get(cmdstr, MemoryStream);
        MemoryStream.Position := 0;
        Memo2.Lines.Clear;
        Memo2.Lines.LoadFromStream(MemoryStream);
        ActionLog('Edit News [R]=>');
        txtAction.Lines.AddStrings(Memo2.Lines);
        ResponseStr := ExtractResponse(Memo2.Lines[0]);

        if (lowercase(ResponseStr.Strings[0]) = '1') then
          begin
          btnHTTP.Click;
          MessageDlg('Success',mtInformation,[mbOK],0);
          end
        Else
          begin
          MessageDlg('Error',mtError,[mbOK],0);
          end;
      except
        MessageDlg('Error: Cannot connect to server !!!', mtError, [mbOK], 0);
        ShowLog('Error: Cannot delete Banner name = ' + dm.tblBanner.fieldbyname('filename').AsString);
        btnHTTP.Click;
        abort;
      end;
    end;
end;

procedure TfrmAdmin.btnCancelBannerClick(Sender: TObject);
begin
  if dm.tblBanner.State in [dsEdit, dsInsert] then
  begin
    dm.tblBanner.Cancel;
  end;
end;

procedure TfrmAdmin.btnBackColorClick(Sender: TObject);
begin
  if ColorDialog1.Execute then
  begin
    with dm.tblConfig do
    begin
      edit;

      FieldByName('news_back_color').AsInteger := ColorDialog1.Color;

      post;
    end;

    btnSaveConf.Click;
  end;
end;

function TfrmAdmin.CompactAndRepair: Boolean; {DB = Path to Access Database}
var
  v: OLEvariant;
begin
  Result := True;
  try
    ActionLog('Compact Databse => '+ ClientDB);
    v := CreateOLEObject('JRO.JetEngine');
    try
      V.CompactDatabase('Provider=Microsoft.Jet.OLEDB.4.0;Jet OLEDB:Database Password=9999;Data Source='+ClientDB,
                        'Provider=Microsoft.Jet.OLEDB.4.0;Jet OLEDB:Database Password=9999;Data Source=dbx.mdb');

      DeleteFile(ClientDB);
      RenameFile('dbx.mdb', ClientDB);
    finally
      V := Unassigned;
    end;
  except
    Result := False;
  end;
end;

procedure TfrmAdmin.btnCompactClick(Sender: TObject);
begin
  If dm.db.Connected then dm.db.Close;

  try
    if CompactAndRepair then
    begin
      MessageDlg('Database has been compact successfully.', mtInformation, [mbOK], 0);
    end
    else
    begin
      MessageDlg('Database compacting error! Close DS and DS Agent before start compact.', mtInformation, [mbOK], 0);
    end;

  finally
    DBconnect;
  end;
end;

procedure TfrmAdmin.btnRegisterClick(Sender: TObject);
begin
{
  if not Registered then
  begin
    MCode := GetHardDiskSerial;
    if InputQuery('Software Registration ...',
        'Please insert License number for Machine No.: ' + MCode, TempReg) then
    begin
      if LicenseOK(TempReg) <> 0 then
      begin
        DoRegister(TempReg);
        Registered := True;
        btnRegister.Visible := False;
        MessageDlg('Thank you for registering our product.', mtInformation, [mbOK], 0);
      end
      else
      begin
        MessageDlg('License number is incorrect !!!', mtError, [mbOK], 0);
      end;
    end;
  end;
}

//  Application.CreateForm(TfrmRegister, frmRegister);
//  frmRegister.show();
end;

function TfrmAdmin.GenDOW: string;
begin
  result := '';

  with dm.tblQ do
  begin
    if FieldByName('ismonday').AsBoolean then
      result := result + '1'
    else
      result := result + '0';
    if FieldByName('istuesday').AsBoolean then
      result := result + '1'
    else
      result := result + '0';
    if FieldByName('iswednesday').AsBoolean then
      result := result + '1'
    else
      result := result + '0';
    if FieldByName('isthursday').AsBoolean then
      result := result + '1'
    else
      result := result + '0';
    if FieldByName('isfriday').AsBoolean then
      result := result + '1'
    else
      result := result + '0';
    if FieldByName('issaturday').AsBoolean then
      result := result + '1'
    else
      result := result + '0';
    if FieldByName('issunday').AsBoolean then
      result := result + '1'
    else
      result := result + '0';
  end;
end;

procedure TfrmAdmin.TranslateDOW(input: string);
begin
  with dm.tblQ do
  begin
    FieldByName('ismonday').AsBoolean := strtoint(input[1]) = 1;
    FieldByName('istuesday').AsBoolean := strtoint(input[2]) = 1;
    FieldByName('iswednesday').AsBoolean := strtoint(input[3]) = 1;
    FieldByName('isthursday').AsBoolean := strtoint(input[4]) = 1;
    FieldByName('isfriday').AsBoolean := strtoint(input[5]) = 1;
    FieldByName('issaturday').AsBoolean := strtoint(input[6]) = 1;
    FieldByName('issunday').AsBoolean := strtoint(input[7]) = 1;
  end;
{
  with dm.tblOldQ do
  begin
    FieldByName('ismonday').AsBoolean := strtoint(input[1]) = 1;
    FieldByName('istuesday').AsBoolean := strtoint(input[2]) = 1;
    FieldByName('iswednesday').AsBoolean := strtoint(input[3]) = 1;
    FieldByName('isthursday').AsBoolean := strtoint(input[4]) = 1;
    FieldByName('isfriday').AsBoolean := strtoint(input[5]) = 1;
    FieldByName('issaturday').AsBoolean := strtoint(input[6]) = 1;
    FieldByName('issunday').AsBoolean := strtoint(input[7]) = 1;
  end;
}
end;

function TfrmAdmin.GenIntervalStr(tinput: TDateTime): string;
var
  ResultInt: integer;
begin
  ResultInt := HourOf(tinput) * 100 + MinuteOf(tinput);
  result := inttostr(ResultInt);
end;

function TfrmAdmin.GenTimeInterval(input: string): TDateTime;
var
  inputint, hh,mm: integer;
begin
  try
    inputint := strtoint(input);
  except
    inputint := 0;
  end;

  hh := inputint div 100;
  mm := inputint mod 100;

  Result := Date + EncodeTime(hh, mm, 0, 0);
end;


procedure TfrmAdmin.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=false;
  Timer1.Interval:=5000;
  iLabelClick:=0;
end;

procedure TfrmAdmin.cboConfTypeChange(Sender: TObject);
begin
  lblStream.Enabled:=false;
  txtStream.Enabled:=false;
  lblStream.Visible:=false;
  txtStream.Visible:=false;
  if (cboConfType.ItemIndex = 2) then
  begin
    lblStream.Enabled:=true;
    txtStream.Enabled:=true;
    lblStream.Visible:=true;
    txtStream.Visible:=true;
  end;

    dm.tblConfig.Edit;
    dm.tblConfig.FieldByName('def_type').asinteger:=cboConfType.ItemIndex;
    dm.tblConfig.Post;
end;

procedure TfrmAdmin.tbConfigEnter(Sender: TObject);
begin
  cboConfType.ItemIndex:=strtoint(dbDefType.Text);
  txtStream.Text:=dbDefFile.Text;
end;

procedure TfrmAdmin.btnClientSelectClick(Sender: TObject);
begin
  ClientDB:=txtClientDB.Text;
  ClientRun:=lstClient.ItemIndex+1;
  DBConnect;
  Application.ProcessMessages;
  btnHttp.Click;
  MainPage.ActivePage := tbConfig;

end;

procedure TfrmAdmin.CreateClientList;
var iniFile: TIniFile;
  iMaxClient: integer;
  iRunClient: integer;
  sData,sName,sCode,sDest,sDB: string;
begin
    iniFile:=TiniFile.Create(GetCurrentDir+'\config.ini');
    try
      iMaxClient:=iniFile.ReadInteger('Client','max',1);
      lstClient.Clear;
      lstClientData.Clear;
      for iRunClient:=1 to iMaxClient do
        begin
        sCode:='Client'+Format('%.2d',[iRunClient]);
        sName:=iniFile.ReadString(sCode,'Name',sCode);
        sDest:=iniFile.ReadString(sCode,'Description',sCode);
        sDB:=iniFile.ReadString(sCode,'File',sCode);
        sData:=format('%s|%s|%s|%s',[sCode,sName,sDest,sDB]);
        if (FileExists(GetCurrentDir+'\'+sDB)) then
          begin
          lstClient.Items.Add(sName);
          lstClientData.Items.Add(sData);
          end;
        if (iRunClient = 1) then
          begin
          txtClientCode.Text:=sCode;
          txtClientName.Text:=sName;
          txtClientDesc.Text:=sDest;
          txtClientDB.Text:=sDB;
          end;
        end;
      lstClient.ItemIndex:=0;
      lstClientData.ItemIndex:=0;
    finally
      iniFile.Free;
    end;

//  frmVideo.Width:=strtoint(txtWidth.Text);
//  frmVideo.Height:=strtoint(txtHeight.Text);
//  frmVideo.Left:=strtoint(txtLEft.Text);
//  frmVideo.Top:=strtoint(txtTop.Text);
end;

procedure TfrmAdmin.lstClientClick(Sender: TObject);
var
  OutPutList: TStringList;
  sValue: String;
begin
  OutPutList := TStringList.Create;
  try
    iClientSelect:=lstClient.ItemIndex;
    sValue:=lstClientData.Items.Strings[iClientSelect];
//    Split('|', sValue, OutPutList);
    ExtractStrings(['|'], [], PChar(sValue),OutputList);
//     Writeln(OutPutList.Text);
//     Readln;
    txtClientCode.Text:=OutputList[0];
    txtClientName.Text:=outputList[1];
    txtClientDesc.Text:=outputList[2];
    txtClientDB.Text:=outputList[3];
  finally
    OutPutList.Free;
  end;
end;

procedure TfrmAdmin.IdHTTP1WorkBegin(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCountMax: Integer);
begin
  txtRequest.Text:=idHTTP1.Host+idHTTP1.Request.URL;
end;

procedure TfrmAdmin.btnAdminSaveClick(Sender: TObject);
begin
  btnSaveConf.Click;
end;

procedure TfrmAdmin.btnAdminLoginClick(Sender: TObject);
begin
  if (btnAdminLogin.Caption = 'Logout') then
    begin
    pagAdmin.ActivePage:=pagAdminMain;
    frameAdmin.Enabled:=false;
    pagAdmin.Visible:=false;
    lblAdminPass.Visible:=true;
    txtAdminPass.Visible:=true;
    txtAdminPass.Text:='';
    btnAdminLogin.Caption:='Login';
    end
  Else
    begin
    if (txtAdminPass.Text = '@revenue') then
      begin
      MessageDlg('Welcome Admin',mtInformation, [mbOK], 0);
      pagAdmin.ActivePage:=pagAdminConfig;
      frameAdmin.Enabled:=true;
      pagAdmin.Visible:=true;
      btnAdminLogin.Caption:='Logout';
      lblAdminPass.Visible:=false;
      txtAdminPass.Visible:=false;
      end
    Else
      begin
      MessageDlg('Wrong Password',mtError, [mbOK], 0);
      end;
    end;
end;

procedure TfrmAdmin.btnClientCancelClick(Sender: TObject);
begin
  btnClientEdit.Visible:=true;
  btnClientSave.visible:=false;
  btnClientCancel.visible:=false;
  txtClientName.ReadOnly:=true;
  txtClientDesc.ReadOnly:=true;
end;

procedure TfrmAdmin.btnClientEditClick(Sender: TObject);
begin
  btnClientEdit.Visible:=false;
  btnClientSave.visible:=true;
  btnClientCancel.visible:=true;
  txtClientName.ReadOnly:=false;
  txtClientDesc.ReadOnly:=false;
end;

procedure TfrmAdmin.btnClientSaveClick(Sender: TObject);
var iniFile: TIniFile;
begin
  iniFile:=TiniFile.Create(sProgramDir+'config.ini');
  with iniFile do
    begin
    try
      WriteString(txtClientCode.Text,'Name',txtClientName.Text);
      WriteString(txtClientCode.Text,'Description',txtClientDesc.Text);
    finally
      Free;
      ShowMessage('Update Completed.');
    end;
  end;
  CreateClientList;
  btnClientCancel.Click;
  lstClient.ItemIndex:=iClientSelect;
  lstClient.setfocus;
end;

procedure TfrmAdmin.btnAdminClearClick(Sender: TObject);
begin
  txtAction.Clear;
end;

procedure TfrmAdmin.txtActionChange(Sender: TObject);
begin
  if (txtAction.Lines.Count > 1000) then txtAction.Lines.Clear;
end;

end.
