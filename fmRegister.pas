unit fmRegister;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,Math, AdvOfficeImage, AdvPicture, AdvGDIPicture, StdCtrls,
  ExtCtrls,DateUtils,strUtils;

type
  TfrmRegister = class(TForm)
    txtMachineID: TEdit;
    txtLicense: TEdit;
    btnCancel: TButton;
    btnRegister: TButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtLicenseChange(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnRegisterClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    function ExpireDate():boolean;
  end;

var
  frmRegister: TfrmRegister;
  bSuccessFul: boolean;
  iCountRegister: integer;
  sLicenseKey: string;
  sComputerID: string;
  sRegComputerID: string;
  sRegLicenseKey: string;
  sRegName01,sRegName02,sRegName03,sRegName04: string;
  pData01,pData02: pChar;
  sShow,sInputKey: string;
  sData01,sData02: string;
  sTmp01,sTmp02: string;

  function GenKey(sLicenseID: string; iProduct: integer): string;
  procedure dll_GetSerialize(out strOut: pChar); external 'DigitalWall.dll';
  procedure dll_GenMD5(const strHash: pChar; Out strOut: pChar);  external 'DigitalWall.dll';
  procedure dll_GetRegistry(const strKey: PChar;Out strOut: PChar); external 'DigitalWall.dll';
  function dll_AddRegistry(const strKey: pChar; const strValue: pChar): boolean; external 'DigitalWall.dll';
  procedure FreePChar(p: Pchar); external 'DigitalWall.dll';

implementation

{$R *.dfm}

function GenKey(sLicenseID: string; iProduct: integer): string;
var
  sLicense:string;
  iLoop,iStep:integer;
begin
  sTmp01:=sLicenseID;
  iStep:=(2*iProduct)+1;
  for iLoop:=0 to iStep do
  begin
    dll_genMD5(pchar(sTmp01),pData01);
    sTmp02:=pData01;
    FreePChar(pData01);
    sTmp01:=ReverseString(sTmp02);
  end;
  sLicense:=sTmp01;
//  sLicense:=GenNewMD5(pchar(sKey02));

  result:=sLicense;
end;


procedure TfrmRegister.FormCreate(Sender: TObject);
begin
  bSuccessFul:=false;
  Self.Left:=ceil((Screen.Width - Self.Width)/2);
  Self.Top:=ceil((Screen.Height - Self.Height)/2);
// InfoExpress Digital Signage (D-Lite) Type=1
// InfoExpress Digital Signage (D-Wall) Type=3

  sRegName01:='ComputerID';
  sRegName02:='LicenseKey';
  dll_getSerialize(pData01);
  sTmp01:=pData01;
  FreePChar(pData01);

  dll_genMD5(pchar(sTmp01),pData01);
  sComputerID:=pData01;
  FreePChar(pData01);
  dll_getRegistry(pchar(sRegName01),pData01);
  sRegComputerID:=pData01;
  FreePChar(pData01);
  dll_getRegistry(pchar(sRegName02),pData01);
  sRegLicenseKey:=pData01;
  FreePChar(pData01);
  sLicenseKey:=GenKey(sComputerID,0);

  if (length(sRegComputerID) >0 ) and (sRegComputerID <> sComputerID) then
    showMessage('Computer ID is not Match.')
  Else
    begin
//    if (true) then
    if (Not ExpireDate) then
      begin
//      Application.CreateForm(TfrmDigitalWall, frmDigitalWall);
//      frmDigitalWall.Show;
      Self.Hide;
      end
    Else
//      Application.Terminate;
    end;
end;

procedure TfrmRegister.FormShow(Sender: TObject);
begin
  txtLicense.Text:='';
  txtMachineID.Text:=sComputerID;
  if (Not ExpireDate) then
    begin
    if (sLicenseKey = sRegLicenseKey) and (sComputerID = sRegComputerID) then
//      timStart.Enabled:=true;
    end
  Else
//    Application.Terminate;
end;

procedure TfrmRegister.txtLicenseChange(Sender: TObject);
begin
  if (btnRegister.Enabled) then btnRegister.Enabled:=false;
  if (Length(txtLicense.Text)>=32) then
    btnRegister.Enabled:=true;
end;

procedure TfrmRegister.btnCancelClick(Sender: TObject);
begin
//  Application.Terminate;

  Self.Close;
end;

procedure TfrmRegister.btnRegisterClick(Sender: TObject);
begin
  btnRegister.Enabled:=false;
  screen.Cursor:=crHourGlass;
  sleep(1000);
  inc(iCountRegister);
  sInputKey:=txtLicense.Text;
  if (sInputKey = sLicenseKey) then
    begin
    screen.Cursor:=crDefault;
    ShowMessage('Thank you for register this program.');
    dll_addRegistry(pchar(sRegName01),pchar(sComputerID));
    dll_addRegistry(pchar(sRegName02),pchar(sLicenseKey));
//    dll_addRegistry('')
    bSuccessFul:=true;
    frmRegister.Close;
//    timStart.Enabled:=true;
    end
  Else
    begin
    screen.Cursor:=crDefault;
    ShowMessage('License Key is not valid. Please Try Again');
    btnRegister.Enabled:=true;
    txtLicense.SelectAll;
    txtLicense.SetFocus;
    end;
//  if (iCountRegister >= 3) then Application.Terminate;
end;

procedure TfrmRegister.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
//  if (not bSuccessFul) then
//    Application.Terminate;
end;

function TfrmRegister.ExpireDate: boolean;
var
  lSearchRec: TSearchRec;
  dtTime,dtExpire,dtNow: TDateTime;
  iExpire: integer;
  sData: string;
//  wYear,wMonth,wDay: word;
begin
  result:=true;
  iExpire:=30;
  // look for your Application Exe-File
  if FindFirst(Application.ExeName, faAnyFile, lSearchRec) = 0 then
    begin
    // extract Date only from File-Date
    dtTime := FileDateToDateTime(lSearchRec.Time);
    FindClose(lSearchRec);
    dtExpire:=IncDay(dtTime,iExpire);
//    dtExpire:=IncDay(dtTime,-1);
    dtNow:=Now;
    sData:='Now => '+ Formatdatetime('dd/mm/yyyy',dtNow);
    sData:=sData+ #10#13+ 'Expire => '+Formatdatetime('dd/mm/yyyy',dtExpire);
    sData:=sData+ #10#13+ 'File => '+Formatdatetime('dd/mm/yyyy',dtTime);
//    showMessage(sData);
    if (dtExpire < dtNow) or (dtTime > dtNow) then
      begin
      ShowMessage('Application is Expired. Thank you.');
      end
    Else
      result:=false;
  end;
end;
end.
