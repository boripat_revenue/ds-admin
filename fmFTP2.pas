unit fmFTP2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, FileCtrl, AdvGlassButton, RXCtrls, ExtCtrls, DB,
  AdvOfficePager, DBCtrls, Mask, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdFTP, ComCtrls, IdAntiFreezeBase,
  IdAntiFreeze, GtroDBDateTimePicker;

type
  TfrmFTP2 = class(TForm)
    AdminPage: TAdvOfficePager;
    AdvOfficePage2: TAdvOfficePage;
    Bevel3: TBevel;
    RxLabel12: TRxLabel;
    Edit1: TEdit;
    btnFile: TAdvGlassButton;
    btnUpload: TAdvGlassButton;
    OpenDialog1: TOpenDialog;
    btnCancel: TAdvGlassButton;
    FTP: TIdFTP;
    ProgressBar1: TProgressBar;
    IdAntiFreeze1: TIdAntiFreeze;
    RxLabel7: TRxLabel;
    DBComboBox1: TDBComboBox;
    DBComboBox2: TDBComboBox;
    RxLabel8: TRxLabel;
    d1: TGtroDBDateTimePicker;
    t1: TGtroDBDateTimePicker;
    t2: TGtroDBDateTimePicker;
    d2: TGtroDBDateTimePicker;
    RxLabel1: TRxLabel;
    RxLabel2: TRxLabel;
    procedure btnFileClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnUploadClick(Sender: TObject);
    procedure FTPWork(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCount: Integer);
    procedure FTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
      const AWorkCountMax: Integer);
    procedure FTPWorkEnd(Sender: TObject; AWorkMode: TWorkMode);
    procedure d1Change(Sender: TObject);
    procedure d2Change(Sender: TObject);
    procedure t1Change(Sender: TObject);
    procedure t2Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetButton(value: boolean);
  end;

var
  frmFTP2: TfrmFTP2;

implementation

uses
  fmAdmin, data;
{$R *.dfm}

procedure TfrmFTP2.SetButton(value: boolean);
begin
  btnFile.Enabled := Value;
  btnUpload.Enabled := Value;
  btnCancel.Enabled := True;
end;

procedure TfrmFTP2.btnCancelClick(Sender: TObject);
begin
  frmAdmin.GetServerBanner;
  Close;
end;

procedure TfrmFTP2.FormShow(Sender: TObject);
var sStart,sStop: string;
begin
  SetButton(True);
  Edit1.Text := '';
  ProgressBar1.Position := 0;
  //DBComboBox1.ItemIndex := -1;
  //DBComboBox2.ItemIndex := -1;

  with dm.tblBanner do
  begin
    if not (state in [dsEdit, dsInsert]) then
    begin

      Append;
      FieldByName('id').AsInteger := TRUNC(frac(Now) * 100000);
      FieldByName('StartDate').AsDateTime := Now;
      FieldByName('StopDate').AsDateTime := Now;
      FieldByName('active').AsBoolean := True;
      FieldByName('showposition').AsInteger := 1;
      FieldByName('showtype').AsInteger := 1;
      post;
{
      d1.Date := Now;
      t1.DateTime := d1.Date;
      t1.Time := strToTime('00:00:01');
      d2.Date := Now;
      t2.DateTime := d2.Date;
      t2.Time := strToTime('23:59:59');
}
      sStart:=formatDateTime('dd/mm/yyyy 00:00:01',Now);
      sStop:=formatDateTime('dd/mm/yyyy 23:59:59',Now);
      d1.DateTime:=strToDateTime(sStart);
      d2.DateTime:=strToDateTime(sStop);
      t1.DateTime:=strToDateTime(sStart);
      t2.DateTime:=strToDateTime(sStop);
    end;
  end;

end;

procedure TfrmFTP2.btnFileClick(Sender: TObject);
var
  myFile: file of Byte;
begin
  if OpenDialog1.Execute then
  begin
    AssignFile(myFile, OpenDialog1.FileName);
    Reset(myFile);
    Edit1.Text := OpenDialog1.FileName;
    ProgressBar1.Max := FileSize(myFile);
    CloseFile(myFile);
  end;
end;

procedure TfrmFTP2.btnUploadClick(Sender: TObject);
var
  cmdstr: string;
  fname, flocation: string;
  bSuccess: boolean;
begin
  bSuccess:=false;
  if edit1.Text = '' then
    begin
    MessageDlg('Error: Please select image file!!!', mtError, [mbOK], 0);
    abort;
    end;

  if (d2.DateTime < Now) then
    begin
      MessageDlg('Error: Stop date must be the future time !!!', mtError, [mbOK], 0);
      abort;
    end;

  if (frac(d2.DateTime) <= frac(d1.DateTime)) then
    begin
    MessageDlg('Error: Stop date must be later than start date !!!', mtError, [mbOK], 0);
    abort;
    end;
  if not FileExists(Edit1.Text) then
    begin
    MessageDlg('Error: File is not exist!!!', mtError, [mbOK], 0);
    abort;
    end;

  if (DBComboBox1.ItemIndex = -1) or (DBComboBox2.ItemIndex = -1) then
  begin
    MessageDlg('Error: Please complete Banner Type and Banner Position before start upload!!!', mtError, [mbOK], 0);
    abort;
  end;

  Selectnext(ActiveControl, true, true);
  SetButton(False);
  //Self.Enabled := False;

  with dm.tblBanner do
  begin
    MemoryStream.Clear;

    cmdstr := frmAdmin.GenHTTPRequest(true, 14, inttostr(client_id)
                    + '&filename=' + frmAdmin.ReplaceSpace(ExtractFileName(Edit1.Text))
                    + '&position=' + DBComboBox2.Text
                    + '&type=' + DBComboBox1.Text
                    + '&startdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('startdate').AsDateTime)
                    + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('startdate').AsDateTime)
                    + '&stopdate=' + FormatDateTime('yyyy-mm-dd', fieldbyname('stopdate').AsDateTime)
                    + '%20' + FormatDateTime('hh:nn:ss', fieldbyname('stopdate').AsDateTime)
                    + '&active=Y');

    try
      frmAdmin.ActionLog('Banner Upload Start [S]:'+cmdstr);
      frmAdmin.IdHTTP1.get(cmdstr, MemoryStream);
      Application.ProcessMessages;

      MemoryStream.Position := 0;
      frmAdmin.Memo2.Lines.Clear;
      frmAdmin.Memo2.Lines.LoadFromStream(MemoryStream);
      frmAdmin.ActionLog('Banner Upload Start [R]=>');
      frmAdmin.txtAction.Lines.AddStrings(frmAdmin.Memo2.Lines);
      Application.ProcessMessages;

    except
      MessageDlg('Error: Cannot connect to server !!!', mtError, [mbOK], 0);
      frmAdmin.ShowLog('Error: Cannot Add File to Server. ' + ExtractFileName(Edit1.Text));
      abort;
    end;

    ResponseStr := frmAdmin.ExtractResponse(frmAdmin.Memo2.Lines[0]);

    if strtoint(ResponseStr[0]) > 0 then
    begin
      flocation := ResponseStr[1];
      fname := ResponseStr[2];

      // ============start FTP part===================
      FTP.Host := ftp_server;
      FTP.Port := ftp_port;
      FTP.Username := ftp_user;
      FTP.Password := ftp_pass;

      try
        FTP.Passive := ftp_passive;
        FTP.Connect(TRUE, 60);
        Application.ProcessMessages;

        FTP.Put(Edit1.Text, flocation + '/' + fname);       // set file name which get from server
        FTP.Disconnect;

        Application.ProcessMessages;
        MessageDlg('FTP Upload Banner finished.', mtInformation, [mbOK], 0);
        frmAdmin.ShowLog('Finish: Upload Banner to Server. ' + flocation
          + '/' + ExtractFileName(Edit1.Text) + ' (' + fname + ')');
        Application.ProcessMessages;

        // sending ftp confirmation message to server
        MemoryStream.Clear;
        cmdstr := frmAdmin.GenHTTPRequest(true, 15, fname);

        try
          frmAdmin.ActionLog('Banner Upload Finish [S]:'+cmdstr);
          frmAdmin.IdHTTP1.get(cmdstr, MemoryStream);
          Application.ProcessMessages;
          MemoryStream.Position := 0;
          frmAdmin.Memo2.Lines.Clear;
          frmAdmin.Memo2.Lines.LoadFromStream(MemoryStream);
          frmAdmin.ActionLog('Banner Upload Start [R]=>');
          frmAdmin.txtAction.Lines.AddStrings(frmAdmin.Memo2.Lines);

          frmAdmin.ShowLog('Finish: Upload Banner Confirmation. ' + flocation + '/'
            + ExtractFileName(Edit1.Text) + ' (' + fname + ')');
          bSuccess:=true;
        except
          MessageDlg('Error: FTP upload Banner confirmation failed !!!', mtError, [mbOK], 0);
          frmAdmin.ShowLog('Error!: Upload Banner Confirmation Failed. ' + flocation + '/'
            + ExtractFileName(Edit1.Text) + ' (' + fname + ')');
          abort;
        end;

      // finish sending ftp confirmation block
      // ================================================================================

      except on E: Exception do
        begin
          if FTP.Connected then FTP.Disconnect;
          Application.ProcessMessages;
          MessageDlg('Error: Cannot connect to server !!!', mtError, [mbok], 0);
          Application.ProcessMessages;
          frmAdmin.ShowLog('Error: Cannot FTP to Server. ' + flocation + '/'
            + ExtractFileName(Edit1.Text) + ' (' + fname + ')');
          Application.ProcessMessages;
        end;
      end;
    end
    else
    begin
      MessageDlg('Error: Upload request - response failed !!!', mtInformation, [mbOK], 0);
      frmAdmin.ShowLog('Error: Upload Request - response error. Upload Banner abort. ' + flocation + '/' + ExtractFileName(Edit1.Text));
    end;
  end;

  frmAdmin.GetServerBanner;
  Self.Close;
  //Self.Enabled := True;
end;

procedure TfrmFTP2.FTPWork(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCount: Integer);
begin
  ProgressBar1.Position := AWorkCount;
  Application.ProcessMessages;
end;

procedure TfrmFTP2.FTPWorkBegin(Sender: TObject; AWorkMode: TWorkMode;
  const AWorkCountMax: Integer);
begin
  ProgressBar1.Position := 0;
  Application.ProcessMessages;
end;

procedure TfrmFTP2.FTPWorkEnd(Sender: TObject; AWorkMode: TWorkMode);
begin
  ProgressBar1.Position := 0;
  Application.ProcessMessages;
end;

procedure TfrmFTP2.d1Change(Sender: TObject);
begin
  t1.DateTime := int(d1.Date) + frac(t1.Time);
end;

procedure TfrmFTP2.d2Change(Sender: TObject);
begin
  t2.DateTime := int(d2.Date) + frac(t2.Time);
end;

procedure TfrmFTP2.t1Change(Sender: TObject);
begin
  d1.DateTime := int(d1.Date) + frac(t1.Time);
end;

procedure TfrmFTP2.t2Change(Sender: TObject);
begin
  d2.DateTime := int(d2.Date) + frac(t2.Time);
end;


end.
